<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\InstrumentColor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="instrument-color-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color_id')->dropDownList(\app\models\Colors::getList(),
        ['prompt' => ' ']) ?>

    <?= $form->field($model, 'product_id')->dropDownList(\app\models\Products::getList(),
        ['prompt' => ' ']) ?>

    <? if($model->id !== null): ?>
        <div class="row">
            <div class="col-md-12 col-xs-3" style="text-align: center">
                <div style="margin-top:20px;">
                    <img src="<?=$model->getImage()?>" alt="" style="width:200px;">
                </div>
            </div>
        </div>
    <?endif;?>

    <?php
    echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
