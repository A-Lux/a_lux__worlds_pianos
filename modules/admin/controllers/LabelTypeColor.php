<?php

namespace app\modules\admin\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelTypeColor
{
    public static function statusList()
    {
        return [
            0 => 'Цвет',
            1 => 'Материал',
        ];
    }

    public static function statusLabel($type)
    {
        switch ($type) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $type), [
            'class' => $class,
        ]);
    }
}