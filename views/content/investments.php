<? $name            = "name".Yii::$app->session["lang"];?>
<? $title           = "title".Yii::$app->session["lang"];?>
<? $content         = "content".Yii::$app->session["lang"];?>
<? $subtitle        = "subtitle".Yii::$app->session["lang"];?>
<? $description     = "description".Yii::$app->session["lang"];?>


    <div class="investments-bread">
        <div class="container">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"> <?=$model->$name;?></li>
              </ol>
            </nav>
          </div>
    </div>
    

    <div class="investments-main">
      <div class="container page-title"><?=$model->$name;?></div>

      <div>
        <div class="col-sm-12 p-0">
          <img src="<?=$banner->getImage();?>" width="100%" alt="">
        </div>
      </div>

      <div class="container page-title article-title"><?=$investments->$title;?></div>

      <div class="article">
        <div class="container">
          <div class="text">
            <?=$investments->$content;?>
          </div>
        </div>
      </div>

      <div class="container page-title article-title"><?=$investments->$subtitle;?></div>

      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <div class="article">
              <div class="text">
                <?=$investments->$description;?>
              </div>
            </div>
          </div>
          <!-- <div class="col-sm-12 col-md-3">
            <img src="<?=$investments->getImage();?>" width="100%" alt="">
          </div> -->
        </div>
      </div>

    </div>
