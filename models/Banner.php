<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property int $menu_id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 * @property string $image
 * @property string $image_background
 * @property string $video
 * @property int $status
 */
class Banner extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/banner/';
    public $pathVideo = 'uploads/video/banner/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'title'], 'required'],
            [['menu_id', 'status'], 'integer'],
            [['text', 'text_en', 'text_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'image', 'image_background', 'video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'menu_id'               => 'Страница баннера',
            'title'                 => 'Заголовок',
            'title_en'              => 'Заголовок на английском',
            'title_kz'              => 'Заголовок на казахском',
            'text'                  => 'Текст',
            'text_en'               => 'Текст на английском',
            'text_kz'               => 'Текст на казахском',
            'image'                 => 'Изображение',
            'image_background'      => 'Фоновое изображение',
            'video'                 => 'Видео',
            'status'                => 'Статус',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banner/' . $this->image : '';
    }

    public function getImageBackground()
    {
        return ($this->image_background) ? '/uploads/images/banner/' . $this->image_background : ' ';
    }

    public function getVideo()
    {
        return ($this->video) ? '/uploads/video/banner/' . $this->video : '';
    }

    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    public function getMenuName()
    {
        return (isset($this->menu))? $this->menu->name: ' ';
    }
}
