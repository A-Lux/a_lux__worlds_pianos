<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GuideSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Путеводитель покупателя';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'title',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100, 'height' => 100]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'background_image',
                'value' => function($data){
                    return Html::img($data->getBackImage(), ['width'=>100, 'height' => 100]);
                }
            ],
//            'title_en',
//            'title_kz',
//            'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',
            //'subtitle',
            //'subtitle_en',
            //'subtitle_kz',
            //'description:ntext',
            //'description_en:ntext',
            //'description_kz:ntext',
            //'image',
            //'background_image',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
