<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\InstrumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Инструменты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instruments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить инструмента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            'name',
            //'name_en',
            //'name_kz',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            //'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',
            //'color',
            //'color_en',
            //'color_kz',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
