<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html var extracted1="item__banner--first" ; class="ie ie7" lang="en">
<![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!-->

<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="shortcut icon" href="/public/images//favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/public/images//apple-touch-icon.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
</head>
<body>
<?php $this->beginBody() ?>

    <!-- HEADER -->
    <?=$this->render('_header')?>
    <!-- END HEADER -->

    <?=$content?>

    <!-- FOOTER -->
    <?=$this->render('_footer')?>
    <!-- END FOOTER -->

<!-- <script>
    var canviLeft = new Canvi({
        content: '.canvi-content',
        navbar: '.canvi-navbar',
        toggle: '.canvi-btn',
        position: 'left',
        pushContent: false,
        isDebug: true,
        responsiveWidths: [{
            breakpoint: "360px",
            width: "320px"
        }, {
            breakpoint: "1466px",
            width: "420px"
        }]
    });
</script> -->

<?php $this->endBody() ?>

</html>

<?php $this->endPage() ?>
