<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PageInstrumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страница инструменты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-instrument-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
//            'title_en',
//            'title_kz',
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return strlen($model->description) > 100 ? Html::encode(substr($model->description, 0, 75)) . "..." : $model->description;
                }
            ],
//            'description_en:ntext',
//            'description_kz:ntext',
            'subtitle',
//            'subtitle_en',
//            'subtitle_kz',
//            'content:ntext',
//            'content_en:ntext',
//            'content_kz:ntext',
//            'text:ntext',
//            'text_en:ntext',
//            'text_kz:ntext',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
