<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Investments */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="investments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'title_kz',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            'subtitle',
            'subtitle_en',
            'subtitle_kz',
            'description:ntext',
            'description_en:ntext',
            'description_kz:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>300]);
                }
            ],
        ],
    ]) ?>

</div>
