<?php

namespace app\controllers;

use app\models\BannerBrand;
use app\models\CategoryInstruments;
use app\models\Colors;
use app\models\Guide;
use app\models\Images;
use app\models\InstrumentColor;
use app\models\Menu;
use app\models\PageInstrument;
use app\models\Products;
use app\models\Spirio;
use Yii;

class CatalogController extends FrontendController
{
    public $layout = "main";

    public function actionInstruments()
    {
        $model = Menu::find()->where('url = "/catalog/instruments"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $instruments = CategoryInstruments::find()->where(['level' => 1])->all();
        $page_instruments = PageInstrument::find()->orderBy('id ASC')->one();
        $images = Images::find()->where(['menu_id' => $model->id])->orderBy('id ASC')->one();
        $guide = Guide::find()->orderBy('id ASC')->one();

        return $this->render('instruments', compact('model', 'instruments', 'page_instruments',
            'images', 'guide'));
    }

    public function actionBrand($url)
    {
        $brand = CategoryInstruments::findOne(['url' => $url]);
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($brand->$metaName, $brand->$metaKey, $brand->$metaDesc);

        $banner = BannerBrand::find()->where(['brand_id' => $brand->id])->all();
        $spirio = Spirio::find()->orderBy('id DESC')->one();

        $grand_piano = Products::find()->where(['category_id' => $brand->id, 'type' => 0])->all();
        $piano = Products::find()->where(['category_id' => $brand->id, 'type' => 1])->all();
        $product = Products::find()->where(['category_id' => $brand->id])->orderBy('type ASC')->all();

        if($brand->id == 1){
            return $this->render('steinway', compact('brand', 'banner', 'product',
                'grand_piano', 'piano', 'spirio'));
        }elseif($brand->id == 2){
            return $this->render('boston', compact('brand', 'banner', 'product',
                'grand_piano', 'piano', 'spirio'));
        }elseif ($brand->id == 3){
            return $this->render('essex', compact('brand', 'banner', 'product',
                'grand_piano', 'piano', 'spirio'));
        }
    }

    public function actionGuide()
    {
        $guide = Guide::find()->orderBy('id ASC')->one();

        return $this->render('guide', compact('guide'));
    }

    public function actionCatalog($id)
    {
        $catalog = CategoryInstruments::findOne(['id' => $id]);

        return $this->render('catalog', compact('catalog'));
    }

    public function actionProduct($id)
    {
        $product = Products::findOne(['id' => $id]);
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($product->$metaName, $product->$metaKey, $product->$metaDesc);

        $instrumentColor = InstrumentColor::find()->where(['product_id' => $product->id])->all();

        $color = [];
        $material = [];
        foreach($instrumentColor as $item){
            if($item->color->type == 0){
                $color[] = $item;
            }else{
                $material[] = $item;
            }
        }

        return $this->render('product-card', compact('product', 'instrumentColor',
            'color', 'material'));
    }

    public function actionTest()
    {
        $model = new Colors();

        $color = $model->getColor();

        var_dump($color);
    }
}