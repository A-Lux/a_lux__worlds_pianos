<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<? $content = "content" . Yii::$app->session["lang"]; ?>
<? $subtitle = "subtitle" . Yii::$app->session["lang"]; ?>
<? $description = "description" . Yii::$app->session["lang"]; ?>


<div class="investments-bread">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?= Yii::$app->view->params['main']; ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"> <?= $guide->$name; ?></li>
            </ol>
        </nav>
    </div>
</div>


<div class="investments-main">
    <div class="container page-title article-title"><?= $guide->$title; ?></div>

    <div class="article">
        <div class="container">
            <div class="text">
                <?= $guide->$content; ?>
            </div>
        </div>
    </div>

    <div class="container page-title article-title"><?= $guide->$subtitle; ?></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-9">
                <div class="article">
                    <div class="text">
                        <?= $guide->$description; ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-3">
                <img src="<?= $guide->getImage(); ?>" width="100%" alt="">
            </div>
        </div>

        <a href="<?= $guide->getFilePath();?>" target="_blank" class="button">Скачать путеводитель</a>
    </div>

</div>
