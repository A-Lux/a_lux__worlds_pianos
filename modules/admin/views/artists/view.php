<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Artists */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Артисты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="artists-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'category_id', 'value'=>function($model){ return $model->categoryName;}],
            'title:ntext',
            'title_en:ntext',
            'title_kz:ntext',
            'name',
            'name_en',
            'name_kz',
            [
                'format' => 'html',
                'attribute' => 'image',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            'url:url',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            'metaName',
            'metaName_en',
            'metaName_kz',
            'metaDesc',
            'metaDesc_en',
            'metaDesc_kz',
            'metaKey',
            'metaKey_en',
            'metaKey_kz',
        ],
    ]) ?>

</div>
