<? $name = "name".Yii::$app->session["lang"];?>
<? $title = "title".Yii::$app->session["lang"];?>
<? $content = "content".Yii::$app->session["lang"];?>
<? $description = "description".Yii::$app->session["lang"];?>
<div class="instruments-bread">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$catalog->$name;?></li>
            </ol>
        </nav>
    </div>
</div>

<div class="catalog-main">
    <div class="container page-title"><?=$catalog->$title;?></div>

    <div class="article">
        <div class="container">
            <div class="text width-text">
               <?=$catalog->$description;?>
            </div>
        </div>
    </div>

    <div class="container">
        <a href="" class="button">Получить более подробную информацию</a>
    </div>

    <div class="container catalog-title">Каталог</div>
    <div class="container catalog-subtitle">Найдите подходящую модель</div>

    <div class="container">
        <div class="catalog-products">
            <div class="row">
                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-2">
                    <div class="item">
                        <div class="img">
                            <img src="/public/images/pictrures-pianino.png" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="title">UP-132E PE Performance Edition</div>
                            <div class="description">Это пианино высотой 132 см является самой большой
                                моделью из серии Boston PE. Инструмент обладает мощным и насыщенным
                                звуком, ни в чем не уступающим некоторым роялям.
                            </div>
                            <a href="#" class="">Узнать больше <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="pag">
            <div class="strip"></div>
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png"
                                                                         alt=""></a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png"
                                                                         alt=""></a></li>
            </ul>
            <div class="strip"></div>
        </div>
    </div>

</div>
