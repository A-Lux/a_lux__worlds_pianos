<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Мета теги</a>
            </li>
            <li class="nav-item">
                <a id="profile_en-tab" data-toggle="tab" href="#profile_en" role="tab" aria-controls="profile_en" aria-selected="false" class="nav-link">Мета теги на казахском</a>
            </li>
            <li class="nav-item">
                <a id="profile_kz-tab" data-toggle="tab" href="#profile_kz" role="tab" aria-controls="profile_kz" aria-selected="false" class="nav-link">Мета теги на английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile_en" role="tabpanel" aria-labelledby="profile_en-tab" class="tab-pane fade">

                <?= $form->field($model, 'metaName_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_en')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_en')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile_kz" role="tabpanel" aria-labelledby="profile_kz-tab" class="tab-pane fade">
                <?= $form->field($model, 'metaName_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_kz')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_kz')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">
                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>
            </div>
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'description_en')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?php
                echo $form->field($model, 'content_en')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'description_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?php
                echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>


            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'category_id')->dropDownList(\app\models\CategoryInstruments::getList(),
                    ['prompt' => ' ']) ?>

                <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <? if($model->id !== null): ?>
                <div class="row">
                    <div class="col-md-12 col-xs-3" style="text-align: center">
                        <div style="margin-top:20px;">
                            <img src="<?=$model->getImage()?>" alt="" style="width:200px;">
                        </div>
                    </div>
                </div>
                <? endif;?>

                <?php
                echo $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <? if($model->id !== null): ?>
                <div class="row">
                    <div class="col-md-12 col-xs-3" style="text-align: center">
                        <div style="margin-top:20px;">
                            <img src="<?=$model->getBackImage()?>" alt="" style="width:200px;">
                        </div>
                    </div>
                </div>
                <? endif;?>

                <?php
                echo $form->field($model, 'background_image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'background_image/*'],
                ]);
                ?>

                <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'width')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'depth')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'type')->dropDownList(
                        [0 => 'Рояль', 1 => 'Пианино']
                ) ?>

                <?php
                echo $form->field($model, 'description')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?php
                echo $form->field($model, 'content')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>


            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
