<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryArtists */

$this->title = 'Добавление категории';
$this->params['breadcrumbs'][] = ['label' => 'Категория артистов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-artists-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
