<?php

use yii\helpers\Url;

?>
<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $text = "text" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<section class="first-block">

    <div class="slider-first">
        <? foreach ($banner as $item): ?>
            <? if ($item->status == 0): ?>
                <div class="slider-item" style="background-image:url('<?=$item->getImageBackground();?>');background-repeat: no-repeat;background-size: cover;">
                    <div class="container ml-xl-0">
                        <div class="row">
                            <div class="info col-sm-5">
                                <div class="logo">
                                    <img src="<?= $logo->getImage(); ?>" alt="">
                                </div>
                                <div class="title"><?= $item->$title; ?></div>
                                <a href="/catalog/instruments" class="">КАТАЛОГ</a>
                            </div>
                            <div class="img col-sm-7">
                                <img src="<?= $item->getImage(); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <video src="<?=$item->getVideo();?>" autoplay controls></video>
            <? endif; ?>
        <? endforeach; ?>
    </div>
</section>


<section class="instruments-cont">
    <div class="container">
        <div class="title"><?=$mainSub[0]->$name;?></div>
        <div class="row">

            <? foreach ($instruments as $item): ?>
                <a href="<?=Url::to(['/catalog/brand', 'url' => $item->url])?>" class="col-10 col-md-4 item">
                    <div class="img">
                        <img src="<?= $item->getImage(); ?>" alt="">
                    </div>
                    <div class="info">
                        <div class="logo-company">
                            <img src="<?= $item->getLogo(); ?>" alt="">
                        </div>
                    </div>
                </a>
            <? endforeach; ?>

        </div>
    </div>
</section>

<section class="offer">
    <div class="container">
        <div class="info">
            <div class="logo">
                <img src="<?= $images->getImage(); ?>" alt="">
            </div>
            <p><?=$mainSub[1]->$name;?></span></p>
            <a href="/content/spirio">Подробнее</a>
        </div>
    </div>
</section>

<!-- <section class="artists">
    <div class="container">
        <div class="title"><?=$mainSub[2]->$name;?></div>
    </div>

    <div class="container">
        <div class="row">

                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="img-cont col-6 p-2">
                            <? for($i = 0; $i < 2; $i++): ?>
                                <?php if(!isset($artists[$i])) continue;?>
                                <div class="<?=$i%2 == 0 ? 'img-b img img-top' : 'img-sm img img-bott'?>">
                                    <div class="img-back">
                                        <img src="<?=$artists[$i]->getImage()?>" class="col-sm-12 p-0" alt="">
                                    </div>
                                    <div class="info">
                                        <a href="" class="text"><?=$artists[$i]->$name?></a>
                                        <span></span>
                                    </div>
                                </div>
                            <?endfor;?>
                        </div>

                        <div class="img-cont col-6 p-2">
                            <? for($i = 2; $i < 4; $i++): ?>
                                <?php if(!isset($artists[$i])) continue;?>
                                <div class="<?=$i%2 == 0 ? 'img-sm img img-top' : 'img-b img img-bott'?>">
                                    <div class="img-back">
                                        <img src="<?=$artists[$i]->getImage()?>" class="col-sm-12 p-0" alt="">
                                    </div>
                                    <div class="info">
                                        <a href="" class="text"><?=$artists[$i]->$name?></a>
                                        <span></span>
                                    </div>
                                </div>
                            <?endfor;?>
                        </div>
                    </div>
                </div>


            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="img-cont col-6 p-2">
                        <? for($i = 4; $i < 6; $i++): ?>
                            <?php if(!isset($artists[$i])) continue;?>
                            <div class="<?=$i%2 == 0 ? 'img-sm img img-top' : 'img-b img img-bott'?>">
                                <div class="img-back">
                                    <img src="<?=$artists[$i]->getImage()?>" class="col-sm-12 p-0" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="text"><?=$artists[$i]->$name?></a>
                                    <span></span>
                                </div>
                            </div>
                        <?endfor;?>
                    </div>

                    <div class="img-cont col-6 p-2">
                        <? for($i = 6; $i < 8; $i++): ?>
                            <?php if(!isset($artists[$i])) continue;?>
                            <div class="<?=$i%2 == 0 ? 'img-b img img-top' : 'img-sm img img-bott'?>">
                                <div class="img-back">
                                    <img src="<?=$artists[$i]->getImage()?>" class="col-sm-12 p-0" alt="">
                                </div>
                                <div class="info">
                                    <a href="" class="text"><?=$artists[$i]->$name?></a>
                                    <span></span>
                                </div>
                            </div>
                        <?endfor;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 -->
