<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_brand".
 *
 * @property int $id
 * @property int $brand_id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 * @property string $image
 * @property string $video
 * @property int $status
 */
class BannerBrand extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/banner_brand/';
    public $pathVideo = 'uploads/video/banner_brand/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'title'], 'required'],
            [['brand_id', 'status'], 'integer'],
            [['text', 'text_en', 'text_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'image', 'video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'brand_id'  => 'Страница баннера',
            'title'     => 'Заголовок',
            'title_en'  => 'Заголовок на английском',
            'title_kz'  => 'Заголовок на казахском',
            'text'      => 'Текст',
            'text_en'   => 'Текст на английском',
            'text_kz'   => 'Текст на казахском',
            'image'     => 'Изображение',
            'video'     => 'Видео',
            'status'    => 'Статус',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banner_brand/' . $this->image : '/no-image.png';
    }

    public function getVideo()
    {
        return ($this->video) ? '/uploads/video/banner_brand/' . $this->video : '';
    }

    public function getBrand()
    {
        return $this->hasOne(CategoryInstruments::className(), ['id' => 'brand_id']);
    }

    public function getBrandName()
    {
        return (isset($this->brand))? $this->brand->name: ' ';
    }
}
