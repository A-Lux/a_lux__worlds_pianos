<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Spirio */

$this->title = 'Spirio';
$this->params['breadcrumbs'][] = ['label' => 'Spirio', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spirio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            'title_en',
            'title_kz',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            'subtitle',
            'subtitle_en',
            'subtitle_kz',
            'text:ntext',
            'text_en:ntext',
            'text_kz:ntext',
        ],
    ]) ?>

</div>
