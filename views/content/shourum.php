<? $name = "name".Yii::$app->session["lang"];?>
    <div class="container timer-bread">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><?=Yii::$app->view->params['main'];?></a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?=$model->$name;?>
            </li>
          </ol>
        </nav>
      </div>

      <div class="container">
          <div class="container page-title"><?=$model->$name;?></div>
      </div>

      <div class="container">
        <div class="description-shou-rum">Шоу рум откроется через</div>
        <div class="timer" id="time" data-time="<?= $time;?>">
            <div id="countdown" class="countdown">
                <div class="countdown-number">
                  <span class="days countdown-time"></span>&nbsp;&nbsp;
                </div>
                <div class="countdown-number">
                  <span class="hours countdown-time"></span>:
                </div>
                <div class="countdown-number">
                  <span class="minutes countdown-time"></span>:
                </div>
                <div class="countdown-number">
                  <span class="seconds countdown-time"></span>
                </div>
              </div>
        </div>
      </div>
