<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Spirio */

$this->title = 'Создать Spirio';
$this->params['breadcrumbs'][] = ['label' => 'Spirio', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spirio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
