<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BannerBrand */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот баннер?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'brand_id', 'value'=>function($model){ return $model->brandName;}],
            'title',
            'title_en',
            'title_kz',
            'text:ntext',
            'text_en:ntext',
            'text_kz:ntext',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelBanner::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'video',
                'value' => !empty($model->video) ? '<video playsinline="playsinline" autoplay="autoplay" muted="" loop="loop" width="300">
                                                        <source src="'.$model->getVideo().'" type="video/mp4">
                                                    </video>' : null,
            ],

        ],
    ]) ?>

</div>
