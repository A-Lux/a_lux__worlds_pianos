<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SidebarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Боковое меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sidebar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'product_id',
                'filter' => \app\models\Products::getList(),
                'value' => function ($model) {
                    return Html::a($model->products->name, ['view', 'id' => $model->products->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'product_image',
                'value' => function ($model) {
                    return (isset($model->products)) ? Html::img($model->products->getImage(), ['width'=>100]): '';
                },
                'format' => 'raw',
            ],
            [
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                        Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align: center'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
