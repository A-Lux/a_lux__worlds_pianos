<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Investments */

$this->title = 'Создать инвестиции';
$this->params['breadcrumbs'][] = ['label' => 'Инвестиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
