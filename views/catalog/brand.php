<? $name = "name".Yii::$app->session["lang"];?>
<? $title = "title".Yii::$app->session["lang"];?>
<? $content = "content".Yii::$app->session["lang"];?>
<? $description = "description".Yii::$app->session["lang"];?>
<? $text = "text".Yii::$app->session["lang"];?>
<?php
use yii\helpers\Url;
?>
<div class="main-brand">
    <div class="first-block">

        <div class="slider-first">
            <?foreach($banner as $item):?>
                <?if($item->status == 0):?>
                    <div class="slider-item">
                <div class="container">
                    <div class="title"><?=$item->$title;?></div>
                    <div class="subtitle">
                        <?=$item->$text;?>
                    </div>
                </div>
            </div>
                <?else: ?>

                <? endif;?>
            <?endforeach;?>

        </div>
    </div>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page">
                    <?=$brand->$name;?>
                </li>
            </ol>
        </nav>
    </div>


    <div class="container page-title">
        <div class="page-title"><?=$brand->$title;?></div>
    </div>


    <div class="article">
        <div class="container">
            <div class="text">
                <?=$brand->$description;?>
            </div>
        </div>
    </div>

    <? if(!empty($brand->children)):?>
        <? $instrument = $brand->children; ?>
        <div class="instruments-gallery">
        <div class="row">
            <?php for ($i = 0; $i < 3; $i++): ?>
                <?php if(!isset($instrument[$i])) continue;?>
                <div class="col-md-4 p-1">
                    <div class="instrument">
                        <div class="img">
                            <img src="<?=$instrument[$i]->getImage();?>" alt="">
                        </div>
                        <div class="text">
                            <a href="<?=Url::to(['/catalog/catalog', 'id' => $instrument[$i]->id])?>"
                               class="title"><?=$instrument[$i]->$name;?></a>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
        <div class="row">
            <?php for ($i = 3; $i < 4; $i++): ?>
            <?php if(!isset($instrument[$i])) continue;?>
                <div class="col-12 p-1">
                    <div class="instrument">
                        <div class="img">
                            <img src="<?=$instrument[$i]->getImage();?>" alt="">
                        </div>
                        <div class="text">
                            <a href="<?=Url::to(['/catalog/catalog', 'id' => $instrument[$i]->id])?>"
                               class="title"><?=$instrument[$i]->$name;?></a>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
    <?endif;?>

    <?= $brand->$content;?>

</div>
</div>
 