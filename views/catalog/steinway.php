<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<? $content = "content" . Yii::$app->session["lang"]; ?>
<? $description = "description" . Yii::$app->session["lang"]; ?>
<?php

use yii\helpers\Url;

?>

<!-- SLIDER -->
<div class="main-brand">
    <div class="first-block">

        <div class="slider-first">
            <? foreach ($banner as $item): ?>
                <div class="slider-item" style="background:url('<?=$item->getImage();?>');">
                    <div class="container">
                        <div class="title"><?=$item->$title;?></div>
                        <div class="subtitle">
                            <?=$item->$text?>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page">
                    <?=$brand->$name;?>
                </li>
            </ol>
        </nav>
    </div>


    <!-- <div class="container page-title">
        <div class="page-title"><?=$brand->$title;?></div>
    </div>

    <div class="article">
        <div class="container">
            <div class="text">
                <?=$brand->$description;?>
            </div>
        </div>
    </div> -->
</div>

<!-- SLIDER-END -->
<!-- STEINWAY -->
<div class="catalog-instruments">
    <div class="row">
        <?php for ($i = 0; $i < 7; $i++): ?>
            <?php if (!isset($grand_piano[$i])) continue; ?>
            <div class="col-xl-<?=$i == 0 ? '12' : '4'?> p-1">
                <div class="instrument-content <?= $i == 0 ? 'instrument-image' : 'instrument-image-content'?>">
                    <a href="<?= Url::to(['/catalog/product', 'id' => $grand_piano[$i]->id]) ?>">
                        <img src="<?=$grand_piano[$i]->getBackImage();?>" alt="">
                        <div class="instrument-text">
                            <p><?=$grand_piano[$i]->$name;?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php endfor; ?>

        <?php for ($i = 0; $i < 2; $i++): ?>
        <?php if (!isset($piano[$i])) continue; ?>
            <div class="col-xl-6 p-1">
            <div class="instrument-content">
                <a href="<?= Url::to(['/catalog/product', 'id' => $piano[$i]->id]) ?>">
                    <img src="<?=$piano[$i]->getBackImage();?>" alt="">
                    <div class="instrument-text">
                        <p><?=$piano[$i]->$name;?></p>
                    </div>
                </a>
            </div>
        </div>
        <?php endfor; ?>

        <div class="col-xl-12 p-1">
            <div class="instrument-content instrument-spirio">
                <a href="<?= Url::to(['/content/spirio']) ?>">
                    <img src="<?=$spirio->getImage();?>" alt="">
                    <div class="instrument-text">
                        <p><?=$spirio->$name;?></p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- STEINWAY-END -->