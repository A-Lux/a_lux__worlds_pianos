<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrument_color".
 *
 * @property int $id
 * @property int $color_id
 * @property int $product_id
 * @property string $image
 */
class InstrumentColor extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/instrument_color/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrument_color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color_id', 'product_id'], 'required'],
            [['color_id', 'product_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color_id' => 'Цвет',
            'product_id' => 'Продукт',
            'image' => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/instrument_color/' . $this->image : '/no-image.png';
    }

    public function getColor()
    {
        return $this->hasOne(Colors::className(), ['id' => 'color_id']);
    }

    public function getColorName()
    {
        return (isset($this->color))? $this->color->name:'Не задан';
    }

    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getProductsName()
    {
        return (isset($this->products))? $this->products->name:'Не задан';
    }
}
