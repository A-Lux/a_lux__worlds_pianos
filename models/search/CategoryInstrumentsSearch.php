<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CategoryInstruments;

/**
 * CategorySearch represents the model behind the search form of `app\models\CategoryInstruments`.
 */
class CategoryInstrumentsSearch extends CategoryInstruments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'level', 'sort', 'status'], 'integer'],
            [['name', 'name_en', 'name_kz', 'url', 'image', 'created_at', 'metaName', 'metaDesc', 'metaKey', 'metaName_en', 'metaName_kz', 'metaKey_en', 'metaKey_kz', 'metaDesc_en', 'metaDesc_kz'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryInstruments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'level' => $this->level,
            'sort' => $this->sort,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_kz', $this->name_kz])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'metaName', $this->metaName])
            ->andFilterWhere(['like', 'metaDesc', $this->metaDesc])
            ->andFilterWhere(['like', 'metaKey', $this->metaKey])
            ->andFilterWhere(['like', 'metaName_en', $this->metaName_en])
            ->andFilterWhere(['like', 'metaName_kz', $this->metaName_kz])
            ->andFilterWhere(['like', 'metaKey_en', $this->metaKey_en])
            ->andFilterWhere(['like', 'metaKey_kz', $this->metaKey_kz])
            ->andFilterWhere(['like', 'metaDesc_en', $this->metaDesc_en])
            ->andFilterWhere(['like', 'metaDesc_kz', $this->metaDesc_kz]);

        return $dataProvider;
    }
}
