<? $name = "name".Yii::$app->session["lang"];?>
<? $content = "content".Yii::$app->session["lang"];?>
<? $description = "description".Yii::$app->session["lang"];?>
    <div class="instruments-bread">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><?=Yii::$app->view->params['main'];?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?=$model->$name;?></li>
          </ol>
        </nav>
      </div>
    </div>

    <div class="news-main">

        <div class="container page-title">НОВОСТИ</div>

      <div class="container">
        <div class="catalog-products">
          <div class="row">

              <? foreach($news['data'] as $item): ?>
            <div class="col-md-6 p-1">
              <div class="item">
                <div class="img">
                  <img src="<?=$item->getImage();?>" width="100%" alt="">
                </div>
                <div class="info">
                  <div class="text">
                      <div class="title"><?=$item->$name;?></div>
                      <div class="description" data-id="<?=$item->id?>">
                          <?= \app\controllers\ContentController::cutStr($item->$description, 190)?>
                      </div>
                  </div>
                  <a href="<?=\yii\helpers\Url::to(['/content/news-card', 'id' => $item->id])?>" class="">MORE <img src="/public/images/right-arrow-YELLOW.png" alt=""></a>
                </div>
              </div>
            </div>
              <? endforeach; ?>

          </div>
        </div>
      </div>

      <div class="container">
          <?= $this->render('/partials/pagination', [
              'pages'=>$news['pagination'],
          ]);?>

<!--          <div class="pag">-->
<!--            <div class="strip"></div>-->
<!--            <ul class="pagination">-->
<!--              <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png" alt=""></a></li>-->
<!--              <li class="page-item"><a class="page-link active" href="#">1</a></li>-->
<!--              <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--              <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--              <li class="page-item"><a class="page-link" href="#">4</a></li>-->
<!--              <li class="page-item"><a class="page-link" href="#">5</a></li>-->
<!--              <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png" alt=""></a></li>-->
<!--            </ul>-->
<!--            <div class="strip"></div>-->
<!--          </div>-->
        </div>

    </div>
