<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BannerBrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры бренда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать баннер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'brand_id',
                'value' => function ($model) {
                    return
                        Html::a($model->brand->name, ['view', 'id' => $model->brand->id]);
                },
                'format' => 'raw',
            ],
            'title',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100, 'height' => 100]);
                }
            ],
            [
                'attribute' => 'status',
                'filter' =>  \app\modules\admin\controllers\LabelBanner::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelBanner::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            //'title_en',
            //'title_kz',
            //'text:ntext',
            //'text_en:ntext',
            //'text_kz:ntext',
            //'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
