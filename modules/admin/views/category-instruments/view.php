<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryInstruments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'parent_id', 'value'=>function($model){ return $model->parentName;}],
            'name',
            'name_en',
            'name_kz',
            'url:url',
            [
                'format' => 'html',
                'attribute' => 'logo',
                'value' => function($data){
                    return Html::img($data->getLogo(), ['width'=>200]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'fon_image',
                'value' => function($data){
                    return Html::img($data->getFonImage(), ['width'=>200]);
                }
            ],
            //'level',
            //'sort',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'title',
            'title_en',
            'title_kz',
            'description',
            'description_en',
            'description_kz',
            'content',
            'content_en',
            'content_kz' ,
            'short_content',
            'short_content_en',
            'short_content_kz',
            //'created_at',
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'metaName_en',
            'metaName_kz',
            'metaKey_en:ntext',
            'metaKey_kz:ntext',
            'metaDesc_en:ntext',
            'metaDesc_kz:ntext',
        ],
    ]) ?>

</div>
