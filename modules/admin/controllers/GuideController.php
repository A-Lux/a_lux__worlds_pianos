<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Guide;
use app\models\search\GuideSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GuideController implements the CRUD actions for Guide model.
 */
class GuideController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Guide models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuideSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Guide model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Guide model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Guide();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $background_image = UploadedFile::getInstance($model, 'background_image');
            $file = UploadedFile::getInstance($model, 'file');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            if($background_image != null) {
                $time = time();
                $background_image->saveAs($model->path . $time . '_' . $background_image->baseName . '.' . $background_image->extension);
                $model->background_image = $time . '_' . $background_image->baseName . '.' . $background_image->extension;
            }

            if($file != null) {
                $time = time();
                $file->saveAs($model->pathFile . $time . '_' . $file->baseName . '.' . $file->extension);
                $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Guide model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldImage = $model->image;
        $oldBackground_image = $model->background_image;
        $oldFile = $model->file;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $background_image = UploadedFile::getInstance($model, 'background_image');
            $file = UploadedFile::getInstance($model, 'file');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    if(file_exists($model->path . $oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }

            if($background_image == null){
                $model->background_image = $oldBackground_image;
            }else{
                $time = time();
                $background_image->saveAs($model->path . $time . '_' . $background_image->baseName . '.' . $background_image->extension);
                $model->background_image = $time . '_' . $background_image->baseName . '.' . $background_image->extension;
                if(!($oldBackground_image == null)){
                    if(file_exists($model->path . $oldImage)) {
                        unlink($model->path . $oldBackground_image);
                    }
                }
            }

            if($file == null){
                $model->file = $oldFile;
            }else{
                $time = time();
                $file->saveAs($model->pathFile . $time . '_' . $file->baseName . '.' . $file->extension);
                $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
                if(!($oldFile == null)){
                    if(file_exists($model->pathFile . $oldFile)) {
                        unlink($model->pathFile . $oldFile);
                    }
                }
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Guide model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteFile($id)
    {
        $model = Guide::findOne($id);

        $model->file = null;

        $model->save(false);

        return $this->redirect(['update?id='.$id]);
    }

    /**
     * Finds the Guide model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Guide the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Guide::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
