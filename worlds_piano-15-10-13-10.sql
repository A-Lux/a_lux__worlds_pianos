-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 15 2019 г., 10:09
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `worlds_piano`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `title`, `description`, `content`) VALUES
(1, 'Title', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_en` text,
  `title_kz` text,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `metaName` varchar(255) DEFAULT NULL,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaDesc_en` text,
  `metaDesc_kz` text,
  `metaKey` text,
  `metaKey_en` text,
  `metaKey_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `artists`
--

INSERT INTO `artists` (`id`, `category_id`, `title`, `title_en`, `title_kz`, `name`, `name_en`, `name_kz`, `image`, `url`, `content`, `content_en`, `content_kz`, `metaName`, `metaName_en`, `metaName_kz`, `metaDesc`, `metaDesc_en`, `metaDesc_kz`, `metaKey`, `metaKey_en`, `metaKey_kz`) VALUES
(1, 1, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570792956_1.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(2, 1, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570792973_2.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(3, 2, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570792987_3.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(4, 2, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570793004_4.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(5, 3, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570793022_5.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(6, 3, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570793037_6.png', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(7, 1, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570793051_7.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', ''),
(8, 3, 'VAN CLIBURN', '', '', 'VAN CLIBURN', '', '', '1570793066_8.jpg', 'van-cliburn', '<pre>\r\nVAN CLIBURN</pre>\r\n', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `text` text,
  `text_en` text,
  `text_kz` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `menu_id`, `title`, `title_en`, `title_kz`, `text`, `text_en`, `text_kz`, `image`, `video`, `status`) VALUES
(1, 1, 'Эксклюзивный дистрибьютор S <span>&</span> S', '', '', '', '', '', '1569317720_pictrures-pianino.png', '1570595914_plane.webm', 0),
(2, 1, 'Эксклюзивный дистрибьютор S <span>&</span> S', '', '', '', '', '', '1569317764_pictrures-pianino.png', NULL, 0),
(3, 6, '!', '', '', '', '', '', '1570597775_banner.png', '', 0),
(4, 2, 'Spirio', '', '', '', '', '', '1570606328_slider.png', '', 0),
(5, 2, 'Spirio', '', '', '', '', '', '', '1570606494_plane.mp4', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_brand`
--

CREATE TABLE `banner_brand` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `text` text,
  `text_en` text,
  `text_kz` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner_brand`
--

INSERT INTO `banner_brand` (`id`, `brand_id`, `title`, `title_en`, `title_kz`, `text`, `text_en`, `text_kz`, `image`, `video`, `status`) VALUES
(1, 3, 'Инструменты BOSTON', '', '', '<p>ФОРТЕПИАНО BOSTON DESIGNED BY STEINWAY &amp; SONS ПОДАРИТ ВАМ ЗВУК И ЭКСПРЕССИЮ ВЫСОЧАЙШЕГО КЛАССА &mdash; КАК ДЛЯ ВИРТУОЗНОЙ ИГРЫ ПЕРЕД ПУБЛИКОЙ, ТАК И ДЛЯ ЗАНЯТИЙ НА ФОРТЕПИАНО В МУЗЫКАЛЬНОЙ ШКОЛЕ ИЛИ ДЛЯ ДОМАШНЕГО МУЗИЦИОНИРОВАНИЯ В КРУГУ ДРУЗЕЙ.</p>\r\n', '', '', '1570600346_csm_Boston_Piano_in_a_home_retouched_fma_b063c7b750.png', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `fon_image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `description` text,
  `description_en` text,
  `description_kz` text,
  `content` text,
  `content_en` text,
  `content_kz` text,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `metaName` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaKey` text,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaKey_en` text,
  `metaKey_kz` text,
  `metaDesc_en` text,
  `metaDesc_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `name_kz` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `metaName` varchar(256) DEFAULT NULL,
  `metaName_en` varchar(256) DEFAULT NULL,
  `metaName_kz` varchar(256) DEFAULT NULL,
  `metaDesc` varchar(255) DEFAULT NULL,
  `metaDesc_en` varchar(255) DEFAULT NULL,
  `metaDesc_kz` varchar(255) DEFAULT NULL,
  `metaKey` varchar(255) DEFAULT NULL,
  `metaKey_en` varchar(255) DEFAULT NULL,
  `metaKey_kz` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `tree`, `lft`, `rgt`, `depth`, `name`, `name_en`, `name_kz`, `url`, `metaName`, `metaName_en`, `metaName_kz`, `metaDesc`, `metaDesc_en`, `metaDesc_kz`, `metaKey`, `metaKey_en`, `metaKey_kz`) VALUES
(1, NULL, 1, 28, 0, 'Главная', NULL, NULL, '/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, 2, 3, 1, 'Spirio', '', '', 'spirio', '', '', '', '', '', '', '', '', ''),
(3, NULL, 4, 17, 1, 'Инструменты', '', '', 'instruments', '', '', '', '', '', '', '', '', ''),
(4, NULL, 5, 10, 2, 'Steinway', '', '', 'steinway', '', '', '', '', '', '', '', '', ''),
(5, NULL, 11, 16, 2, 'Boston', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, NULL, 18, 19, 1, 'Артисты', '', '', 'artist', '', '', '', '', '', '', '', '', ''),
(7, NULL, 20, 21, 1, 'ШОУ РУМ', '', '', 'show-rum', '', '', '', '', '', '', '', '', ''),
(8, NULL, 22, 23, 1, 'Инвестиции', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, NULL, 24, 25, 1, 'Контакты', '', '', 'contacts', '', '', '', '', '', '', '', '', ''),
(10, NULL, 26, 27, 1, 'Новости и события', '', '', 'novosti', '', '', '', '', '', '', '', '', ''),
(11, NULL, 6, 7, 3, 'Рояли', '', '', 'royali', '', '', '', '', '', '', '', '', ''),
(12, NULL, 8, 9, 3, 'Пианино', '', '', 'piaonino', '', '', '', '', '', '', '', '', ''),
(13, NULL, 12, 13, 3, 'Рояли', '', '', 'royali', '', '', '', '', '', '', '', '', ''),
(14, NULL, 14, 15, 3, 'Пианино', '', '', 'piaonino', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `category_artists`
--

CREATE TABLE `category_artists` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_artists`
--

INSERT INTO `category_artists` (`id`, `name`, `name_en`, `name_kz`) VALUES
(1, 'сольный исполнитель', 'solo artist', 'жеке әртіс'),
(2, 'ансамбли', 'ensembles', 'ансамбльдер'),
(3, 'бессмертные', 'immortals', 'мәңгілік');

-- --------------------------------------------------------

--
-- Структура таблицы `category_instruments`
--

CREATE TABLE `category_instruments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `fon_image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `description` text,
  `description_en` text,
  `description_kz` text,
  `content` text,
  `content_en` text,
  `content_kz` text,
  `short_content` text,
  `short_content_en` text,
  `short_content_kz` text,
  `level` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `metaName` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaKey` text,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaKey_en` text,
  `metaKey_kz` text,
  `metaDesc_en` text,
  `metaDesc_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_instruments`
--

INSERT INTO `category_instruments` (`id`, `parent_id`, `name`, `name_en`, `name_kz`, `url`, `logo`, `image`, `fon_image`, `title`, `title_en`, `title_kz`, `description`, `description_en`, `description_kz`, `content`, `content_en`, `content_kz`, `short_content`, `short_content_en`, `short_content_kz`, `level`, `sort`, `status`, `created_at`, `metaName`, `metaDesc`, `metaKey`, `metaName_en`, `metaName_kz`, `metaKey_en`, `metaKey_kz`, `metaDesc_en`, `metaDesc_kz`) VALUES
(1, NULL, 'Steinway', '', '', 'steinway', '1570509493_pian-1.png', '1570509786_pictrures-pianino.png', '1570519135_instrument1.png', '', '', '', '', '', '', '', '', '', 'Steinway был и остатется самым предпочитаемым инструментом среди профессиональных\r\n                            пианистов, многочисленных музыкантов, артистов и слушателей во всем мире', '', '', 1, 1, 1, '2019-10-11 12:55:05', '', '', '', '', '', '', '', '', ''),
(2, NULL, 'Boston', '', '', 'boston', '1570509831_pian-2.png', '1570509831_pictrures-pianino.png', '1570519176_instrument2.png', 'РОЯЛИ И ПИАНИНО BOSTON', '', '', '<p>Точность ощущения воспроизведения, единение. Свой инструмент вы узнаете по тому как он позволит выразить ваше музыкальное видение - будто ничего нет в воздухе между вашими пальцами и музыкой. Откройте для себя все возможности инструмента семейства STEINWAY. Рояль или пианино BOSTON &ndash; отличный выбор не только для любителей музыки, но и для преподавателей игры на фортепиано, а также для музыкальных учреждений со средним бюджетом.</p>\r\n', '', '', '<div class=\"container article-title\">BOSTON DESIGNED BY STEINWAY &amp; SONS</div>\r\n\r\n<div class=\"article\">\r\n<div class=\"container\">\r\n<div class=\"text width-text\">Благодаря внедренным компанией STEINWAY &amp; SONS передовым техническим разработкам и инновациям (таким как запатентованная конструкция вирбельбанка Octagrip&trade; и дуплексная система Duplex-Skala) рояли и пианино BOSTON по своим звуковым и функциональным характеристикам существенно опережают аналогичные инструменты в среднем ценовом сегменте. Не удивительно, что своим выдающимся качеством инструменты BOSTON завоевывают все больше доверия среди музыкальных учреждений и любителей фортепианной музыки.</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container article-title\">BOSTON DESIGNED BY STEINWAY &amp; SONS</div>\r\n\r\n<div class=\"article\">\r\n<div class=\"container\">\r\n<div class=\"text width-text\">Благодаря внедренным компанией STEINWAY &amp; SONS передовым техническим разработкам и инновациям (таким как запатентованная конструкция вирбельбанка Octagrip&trade; и дуплексная система Duplex-Skala) рояли и пианино BOSTON по своим звуковым и функциональным характеристикам существенно опережают аналогичные инструменты в среднем ценовом сегменте. Не удивительно, что своим выдающимся качеством инструменты BOSTON завоевывают все больше доверия среди музыкальных учреждений и любителей фортепианной музыки.</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container article-title\">BOSTON DESIGNED BY STEINWAY &amp; SONS</div>\r\n\r\n<div class=\"article\">\r\n<div class=\"container\">\r\n<div class=\"text width-text\">Благодаря внедренным компанией STEINWAY &amp; SONS передовым техническим разработкам и инновациям (таким как запатентованная конструкция вирбельбанка Octagrip&trade; и дуплексная система Duplex-Skala) рояли и пианино BOSTON по своим звуковым и функциональным характеристикам существенно опережают аналогичные инструменты в среднем ценовом сегменте. Не удивительно, что своим выдающимся качеством инструменты BOSTON завоевывают все больше доверия среди музыкальных учреждений и любителей фортепианной музыки.</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container article-title\">BOSTON DESIGNED BY STEINWAY &amp; SONS</div>\r\n\r\n<div class=\"article\">\r\n<div class=\"container\">\r\n<div class=\"text width-text\">Благодаря внедренным компанией STEINWAY &amp; SONS передовым техническим разработкам и инновациям (таким как запатентованная конструкция вирбельбанка Octagrip&trade; и дуплексная система Duplex-Skala) рояли и пианино BOSTON по своим звуковым и функциональным характеристикам существенно опережают аналогичные инструменты в среднем ценовом сегменте. Не удивительно, что своим выдающимся качеством инструменты BOSTON завоевывают все больше доверия среди музыкальных учреждений и любителей фортепианной музыки.</div>\r\n</div>\r\n</div>\r\n', '', '', '<p>Фортепиано Boston designed by Steinway &amp; Sons позволяет приобрести профессиональный исполнительский опыт по неожиданной цене.</p>\r\n', '', '', 1, 2, 1, '2019-10-11 12:55:27', '', '', '', '', '', '', '', '', ''),
(3, NULL, 'ESSEX', '', '', 'essex', '1570509603_pian-3.png', '1570509603_pictrures-pianino.png', '1570519191_instrument3.png', '', '', '', '', '', '', '', '', '', '<p>Essex designed by Steinway &mdash; идеальный инcтрумент для первого знакомства с семейством Steinway, разработанный с соблюдением традиционных стандартов качества Steinway.</p>\r\n', '', '', 1, 3, 1, '2019-10-11 12:55:40', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `type`, `name`, `image`) VALUES
(1, 0, 'цвет', '1570703258_Screenshot_3.png'),
(2, 0, 'березовый', '1570703354_Прямоугольник 2 копия 5.png'),
(3, 1, 'черное дерево', '1570802231_Прямоугольник 2.png');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `whatsap` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `address`, `email`, `phone`, `facebook`, `instagram`, `whatsap`, `youtube`, `twitter`) VALUES
(1, '', 'info@gmail.com', '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `emailforrequest`
--

CREATE TABLE `emailforrequest` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `emailforrequest`
--

INSERT INTO `emailforrequest` (`id`, `email`) VALUES
(1, 'kkokoneor@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `guide`
--

CREATE TABLE `guide` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `short_content` text NOT NULL,
  `short_content_en` text,
  `short_content_kz` text,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `subtitle` varchar(255) NOT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `image` varchar(255) DEFAULT NULL,
  `background_image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `guide`
--

INSERT INTO `guide` (`id`, `title`, `title_en`, `title_kz`, `name`, `name_en`, `name_kz`, `short_content`, `short_content_en`, `short_content_kz`, `content`, `content_en`, `content_kz`, `subtitle`, `subtitle_en`, `subtitle_kz`, `description`, `description_en`, `description_kz`, `image`, `background_image`) VALUES
(1, ' путеводитель покупателя', '', '', 'путеводитель покупателя', '', '', '<p>Сомневаетесь, какой инструмент вам подойдет? Воспользуйтесь нашим бесплатным путеводителем покупателя.</p>\r\n', '', '', '<p>Сомневаетесь, какой инструмент вам подойдет? Воспользуйтесь нашим бесплатным путеводителем покупателя.</p>\r\n', '', '', 'Сомневаетесь, какой инструмент вам подойдет? Воспользуйтесь нашим бесплатным путеводителем                             покупателя.', '', '', '\r\nСомневаетесь, какой инструмент вам подойдет? Воспользуйтесь нашим бесплатным путеводителем\r\n    покупателя\r\n', '', '', '1570796480_instrument5.png', '1570796480_instrument4.png');

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `menu_id`, `image`, `note`, `sort`) VALUES
(1, 2, '1569320594_фото1.png', 'фото между текстами', 1),
(2, 2, '1570515294_фото2.png', 'фото между текстами', 2),
(3, 2, '1570515307_фото3.png', 'фото между текстами', 3),
(4, 2, '1570515317_фото4.png', 'фото между текстами', 4),
(5, 3, '1570694082_instrument5.png', 'страница инструменты', 5),
(6, 1, '1570792721_pian-1.png', 'спирио', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `instruments`
--

CREATE TABLE `instruments` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_kz` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_en` text NOT NULL,
  `content_kz` text NOT NULL,
  `color` varchar(255) NOT NULL,
  `color_en` varchar(255) NOT NULL,
  `color_kz` varchar(255) NOT NULL,
  `metaName` varchar(255) DEFAULT NULL,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaDesc_en` text,
  `metaDesc_kz` text,
  `metaKey` text,
  `metaKey_en` text,
  `metaKey_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `instrument_color`
--

CREATE TABLE `instrument_color` (
  `id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `instrument_color`
--

INSERT INTO `instrument_color` (`id`, `color_id`, `product_id`, `image`) VALUES
(1, 1, 1, '1570801160_mini.jpg'),
(2, 1, 1, '1570802161_Boston_PE_126_Fallboard_new_blue_square.png'),
(3, 3, 1, '1570802270_slider.png');

-- --------------------------------------------------------

--
-- Структура таблицы `investments`
--

CREATE TABLE `investments` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `subtitle` varchar(255) NOT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `investments`
--

INSERT INTO `investments` (`id`, `title`, `title_en`, `title_kz`, `content`, `content_en`, `content_kz`, `subtitle`, `subtitle_en`, `subtitle_kz`, `description`, `description_en`, `description_kz`, `image`) VALUES
(1, 'YOUR STEINWAY INVESTMENT', '', '', '<p>An investment in a Steinway is a sound investment for the future. When you own a Steinway &amp; Sons Piano, you have acquired a precious heirloom for future generations...<br />\r\n<br />\r\nResale value<br />\r\n<br />\r\nIn the unlikely event that you should ever want to part with your Steinway, you will not have difficulty in finding a buyer. No other instrument offers the fundamental durability that makes the repair of almost every used Steinway a worthwhile investment. What is more, no other instrument is as desirable on the second-hand market, where it is capable of commanding 25-30% of the current list price even after 30 years of use. Steinway pianos have passed down generations within a family, and yet commanded an unbelievable price when sold, simply because, a Steinway appreciates over time. It is an investment as much as it is an instrument of unimpeachable quality.</p>\r\n', '', '', 'STEINWAY PROMISE', '', '', '<p>When you purchase your Boston or Essex piano from us, you become eligible for a benefit known as the Steinway Promise. This promise grants you ten years to take advantage of receiving full trade-in value on your Boston or Essex piano (in saleable condition) towards a new Steinway grand piano.<br />\r\n<br />\r\n<br />\r\nIf you have ever hoped and dreamed of moving up to a Steinway grand, we are about to present you with the best opportunity you may ever have to exercise that option.</p>\r\n', '', NULL, '1570525831_steinway_promise_sqaure.png');

-- --------------------------------------------------------

--
-- Структура таблицы `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `copyright_en` varchar(255) DEFAULT NULL,
  `copyright_kz` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `logo`
--

INSERT INTO `logo` (`id`, `image`, `copyright`, `copyright_en`, `copyright_kz`) VALUES
(1, '1569386907_header-logo.png', '© 2019 <span>Steinway & Sons</span>                    Название Steinway и изображение лиры являются                    зарегистрированными товарными знаками', '', ''),
(2, '1569386933_banner-logo.png', 'логотип для баннера', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mainsub`
--

CREATE TABLE `mainsub` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mainsub`
--

INSERT INTO `mainsub` (`id`, `name`, `name_en`, `name_kz`) VALUES
(1, 'инструменты', '', ''),
(2, 'Познакомьтесь с системой <span>SPIRIO</span>', '', ''),
(3, 'АРТИСТЫ <span>STEINWAY</span>', '', ''),
(4, 'Записывайтесь             <br>на визит в шоу рум -             <br><span>получайте каталог</span>', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `name_kz` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `metaName` varchar(256) DEFAULT NULL,
  `metaName_en` varchar(256) DEFAULT NULL,
  `metaName_kz` varchar(256) DEFAULT NULL,
  `metaDesc` varchar(255) DEFAULT NULL,
  `metaDesc_en` varchar(255) DEFAULT NULL,
  `metaDesc_kz` varchar(255) DEFAULT NULL,
  `metaKey` varchar(255) DEFAULT NULL,
  `metaKey_en` varchar(255) DEFAULT NULL,
  `metaKey_kz` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `name_en`, `name_kz`, `url`, `metaName`, `metaName_en`, `metaName_kz`, `metaDesc`, `metaDesc_en`, `metaDesc_kz`, `metaKey`, `metaKey_en`, `metaKey_kz`, `status`, `sort`) VALUES
(1, 'Главная', '', '', '/', '', '', '', '', '', '', '', '', '', 0, 1),
(2, 'Spirio', '', '', '/content/spirio', '', '', '', '', '', '', '', '', '', 1, 2),
(3, 'Инструменты', '', '', '/catalog/instruments', '', '', '', '', '', '', '', '', '', 1, 3),
(4, 'Артисты', '', '', '/content/artists', '', '', '', '', '', '', '', '', '', 1, 4),
(5, 'ШОУ РУМ', '', '', '/content/shourum', '', '', '', '', '', '', '', '', '', 1, 5),
(6, 'Инвестиции', '', '', '/content/investments', '', '', '', '', '', '', '', '', '', 1, 6),
(7, 'Новости и события', '', '', '/content/news', '', '', '', '', '', '', '', '', '', 1, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1568089880),
('m190910_042421_catalog', 1568089884);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `date` date NOT NULL,
  `metaName` varchar(255) DEFAULT NULL,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaDesc_en` text,
  `metaDesc_kz` text,
  `metaKey` text,
  `metaKey_en` text,
  `metaKey_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `name`, `name_en`, `name_kz`, `image`, `content`, `content_en`, `content_kz`, `description`, `description_en`, `description_kz`, `date`, `metaName`, `metaName_en`, `metaName_kz`, `metaDesc`, `metaDesc_en`, `metaDesc_kz`, `metaKey`, `metaKey_en`, `metaKey_kz`) VALUES
(1, 'История одного солиста', '', '', '1569308660_instrument4.png', '<p>История одного солиста</p>\r\n', '', '', '<p>История одного солиста</p>\r\n', '', '', '2019-09-17', '', '', '', '', '', '', '', '', ''),
(2, 'Lang Lang performs Mozart’s          Piano Sonata No. 16 in C on the Black          Diamond Limited Edition', '', '', '1570526988_sled-lang-lang-paris-2019_1101x1101.png', '<p>Each year, the film industry celebrates its stars and honors the &ldquo;best of the best&rdquo; in a festive gala. This year, Lady Gaga won the much sought-after Oscar in the category &ldquo;Best Song.&rdquo; Gaga grew up playing her family&rsquo;s BOSTON piano designed by STEINWAY and now owns a STEINWAY piano: a constant musical companion and inspiration in her career. She received a coveted Oscar for her song &ldquo;Shallow&quot; from the musical romantic drama &ldquo;A Star Is Born,&rdquo; in which she also played the female lead role alongside actor and director Bradley Cooper.<br />\r\n<br />\r\nAt the Oscars ceremony, the two impressed viewers with an emotional performance of this award-winning song on a walnut STEINWAY grand piano, which matched the stage design. See here the spectacular performance at the Oscar ceremony.</p>\r\n', '', '', '<p>Steinway dominates at the Tchaikovsky competition Steinway dominates at the Tchaikovsky competition Steinway dominates at the Tchaikovsky competition</p>\r\n', '', '', '2019-10-07', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `news_images`
--

CREATE TABLE `news_images` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news_images`
--

INSERT INTO `news_images` (`id`, `news_id`, `name`, `name_en`, `name_kz`, `image`) VALUES
(1, 1, 'Photos: ©Aaron Poole/Phil McCarten, A.M.P.A.S.', '', '', '1571122755_news-photo1.png'),
(2, 1, 'Photos: ©Aaron Poole/Phil McCarten, A.M.P.A.S.', '', '', '1571123124_news-photo2.png');

-- --------------------------------------------------------

--
-- Структура таблицы `page_instrument`
--

CREATE TABLE `page_instrument` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `subtitle` varchar(255) DEFAULT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  `content` text,
  `content_en` text,
  `content_kz` text,
  `text` text,
  `text_en` text,
  `text_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_instrument`
--

INSERT INTO `page_instrument` (`id`, `title`, `title_en`, `title_kz`, `description`, `description_en`, `description_kz`, `subtitle`, `subtitle_en`, `subtitle_kz`, `content`, `content_en`, `content_kz`, `text`, `text_en`, `text_kz`) VALUES
(1, 'ИНСТРУМЕНТЫ', '', '', '<p>Компания STEINWAY &amp; SONS с 1853 года производит лучшие в мире рояли и пианино. И ей раз за разом удается вносить новизну в звучание, душе, красоту и ценность рояля или пианино.<br />\r\nВ ассортименте STEINWAY для каждого найдется подходящий инструмент: от легендарных STEINWAY до BOSTON для амбициозных пианистов и ESSEX для начинающих.</p>\r\n', '', '', 'ПОЗНАКОМЬТЕСЬ С СИСТЕМОЙ SPIRIO', '', '', '<p>Хотите насладиться игрой Юджи Ванг в своем доме? Воспользуйтесь системой Steinway Spirio.<br />\r\nSteinway Spirio &mdash; первая самовоспроизводящая система со звуком высокого разрешения, достойная легендарного имени Steinway &amp; Sons.</p>\r\n', '', '', '<div class=\"comment\">&laquo;Я слушал и изумлялся. Мне казалось, будто это во сне!.. Теперь, купив рояль, вам уже не нужно приглашать пианиста, чтобы он сыграл в вашем доме&raquo;.</div>\r\n\r\n<div class=\"info\">\r\n<div class=\"author\">ЛАНГ ЛАНГ</div>\r\n\r\n<div class=\"text\">О СИСТЕМЕ STEINWAY &amp; SONS SPIRIO</div>\r\n</div>\r\n', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_kz` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `background_image` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `description_en` text,
  `description_kz` text,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `height` varchar(128) DEFAULT NULL,
  `width` varchar(128) DEFAULT NULL,
  `depth` varchar(128) DEFAULT NULL,
  `weight` varchar(128) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `metaName` varchar(255) DEFAULT NULL,
  `metaName_en` varchar(255) DEFAULT NULL,
  `metaName_kz` varchar(255) DEFAULT NULL,
  `metaDesc` text,
  `metaDesc_en` text,
  `metaDesc_kz` text,
  `metaKey` text,
  `metaKey_en` text,
  `metaKey_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `category_id`, `model`, `name`, `name_en`, `name_kz`, `image`, `background_image`, `description`, `description_en`, `description_kz`, `content`, `content_en`, `content_kz`, `height`, `width`, `depth`, `weight`, `type`, `metaName`, `metaName_en`, `metaName_kz`, `metaDesc`, `metaDesc_en`, `metaDesc_kz`, `metaKey`, `metaKey_en`, `metaKey_kz`) VALUES
(1, 1, 'grand concert ', 'D-274 ', '', '', '1570777138_pictrures-pianino.png', '1570798131_STS_rainbow_collection_xl_rectangle.png', '', '', '', '<p>ф</p>\r\n', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', ''),
(2, 2, 'Performance Edition ', 'GP-215 PE ', '', '', '1570777281_mini.jpg', NULL, '', NULL, NULL, '<p>Этот концертный рояль длиной 215 см является самой большой моделью из линейки Boston. Как и все рояли Boston, он оснащен запатентованной конструкцией вирбельбанка Octagrip&trade; и футором из клена. Эта модель отлично подходит для музыкальных учреждений, профессиональных пианистов и любителей музыки.</p>\r\n', '', '', '', '155 ', '215 ', '404 ', '0', '', '', '', '', '', '', '', '', ''),
(3, 3, 'Classic Studio ', 'EUP - 116 E ', '', '', '1570777444_pictrures-pianino.png', NULL, '<p>Это пианино высотой 132 см является самой большой моделью из серии Boston PE. Инструмент обладает мощным и насыщенным звуком, ни в чем не уступающим некоторым роялям.</p>\r\n', '', '', '<p>Классический дизайн и широкая палитра облицовочных материалов позволяют гармонично вписать этот инструмент в любой интерьер.</p>\r\n', '', '', '116 ', '153 ', '64 ', '236 ', '1', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `record`
--

CREATE TABLE `record` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `record`
--

INSERT INTO `record` (`id`, `name`, `email`, `phone`, `created_at`) VALUES
(1, 'Amir', 'amir@email.com', '8(123) 123-1233', '2019-10-09 18:42:37');

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `request`
--

INSERT INTO `request` (`id`, `name`, `phone`, `date`) VALUES
(1, 'asdasd', '8(123) 123-1231', '2019-10-11 19:17:54');

-- --------------------------------------------------------

--
-- Структура таблицы `spirio`
--

CREATE TABLE `spirio` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_kz` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `content_en` text,
  `content_kz` text,
  `image` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL,
  `subtitle_en` varchar(255) DEFAULT NULL,
  `subtitle_kz` varchar(255) DEFAULT NULL,
  `text` text NOT NULL,
  `text_en` text,
  `text_kz` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `spirio`
--

INSERT INTO `spirio` (`id`, `name`, `title`, `title_en`, `title_kz`, `content`, `content_en`, `content_kz`, `image`, `subtitle`, `subtitle_en`, `subtitle_kz`, `text`, `text_en`, `text_kz`) VALUES
(1, 'Spirio', 'STEINWAY SPIRIO - ПРЕВРАТИТЕ          СВОЙ ДОМ В КОНЦЕРТНЫЙ ЗАЛ', '', '', '<p>Хотите насладиться игрой Юджи Ванг в своем доме?<br />\r\nВоспользуйтесь системой STEINWAY SPIRIO.<br />\r\n<br />\r\nSTEINWAY SPIRIO &mdash; первая самовоспроизводящая система со звуком высокого разрешения, достойная легендарного имени STEINWAY &amp; SONS. Этот шедевр, объединивший в себе мастерство ручного труда и прецизионную механику, передовые технологии и высочайшее качество, подарит неповторимое звучание, которое ни в чем не уступит живому исполнению. Одно касание к дисплею iPad&reg; &mdash; и ваш STEINWAY оживет и заиграет, как настоящий пианист- виртуоз.<br />\r\n<br />\r\nСотни лучших в мире записей Артистов STEINWAY ожидают, чтобы подарить вам чудесные моменты наслаждения музыкой. В вашем распоряжении &mdash; богатая библиотека музыкальных произведений, записанных в студиях STEINWAY. Каталог записей ежемесячно пополняется в автоматическом режиме. От Иоганна Баха до Джорджа Гершвина или Билли Джоэл,любое произведение зазвучит в вашей гостиной так, что вы поверите в то, что сам пианист играет сейчас за вашим роялем.<br />\r\n<br />\r\nВыбирайте то, что вам по душе: классику, поп, танго, буги-вуги или соул- Моцарта, Чайковского, Битлз, Коулдплей или Адель.<br />\r\n<br />\r\nНо это еще не все: вы можете использовать самоиграющий рояль STEINWAY &amp; SONS как обычный рояль STEINWAY. Музицируйте сами &mdash; или попросите Spirio сыграть вместо вас.</p>\r\n', '', '', '1570790570_instrument1.png', 'ВДОХНОВЕНИЕ ПЛЮС ТЕХНОЛОГИИ', '', '', '<p>Благодаря специальной компьютерной программе, которая фиксирует темп движения молоточков (до 1020 динамических уровней при скорости до 800 сигналов в секунду) и положение педалей в момент поднятия демпферов и смещения молоточков (до 256 положений при скорости до 100 сигналов в секунду), система Spirio способна уловить самые тончайшие нюансы и максимально передать экспрессию исполнителя.<br />\r\n<br />\r\nSPIRIO мастерски воспроизведет весь эмоциональный и технический диапазон исполнительского мастерства пианиста, от нежных трелей до точных акцентов или фортиссимо.</p>\r\n', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `created_at`, `updated_at`) VALUES
(1, 'demo@site.com', 'Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v', '$2y$13$qps2yQdQHv8/hXGzGavLQuYElgvcZAbcNZ50HAdfKBdMAFT546ECy', NULL, 'demo@site.com', 1, 1533018523, 1564727410);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banner_brand`
--
ALTER TABLE `banner_brand`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category_artists`
--
ALTER TABLE `category_artists`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category_instruments`
--
ALTER TABLE `category_instruments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `emailforrequest`
--
ALTER TABLE `emailforrequest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `guide`
--
ALTER TABLE `guide`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `instruments`
--
ALTER TABLE `instruments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `instrument_color`
--
ALTER TABLE `instrument_color`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `investments`
--
ALTER TABLE `investments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mainsub`
--
ALTER TABLE `mainsub`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_images`
--
ALTER TABLE `news_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `page_instrument`
--
ALTER TABLE `page_instrument`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `spirio`
--
ALTER TABLE `spirio`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `banner_brand`
--
ALTER TABLE `banner_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `category_artists`
--
ALTER TABLE `category_artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `category_instruments`
--
ALTER TABLE `category_instruments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `emailforrequest`
--
ALTER TABLE `emailforrequest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `guide`
--
ALTER TABLE `guide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `instruments`
--
ALTER TABLE `instruments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `instrument_color`
--
ALTER TABLE `instrument_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `investments`
--
ALTER TABLE `investments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `mainsub`
--
ALTER TABLE `mainsub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `news_images`
--
ALTER TABLE `news_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `page_instrument`
--
ALTER TABLE `page_instrument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `record`
--
ALTER TABLE `record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `spirio`
--
ALTER TABLE `spirio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
