<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $url
 * @property string $logo
 * @property string $image
 * @property string $fon_image
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $short_content
 * @property string $short_content_en
 * @property string $short_content_kz
 * @property int $level
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaKey_en
 * @property string $metaKey_kz
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 */
class CategoryInstruments extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/category/';
    public $brand = 'Бренд';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_instruments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'level', 'sort', 'status'], 'integer'],
            [['name', 'status'], 'required'],
            [['created_at'], 'safe'],
            [[
                'description',  'description_en',   'description_kz',
                'content',      'content_en',       'content_kz',
                'short_content','short_content_en', 'short_content_kz',
                'metaDesc',     'metaDesc_en',      'metaDesc_kz',
                'metaKey',      'metaKey_en',       'metaKey_kz'
            ], 'string'],
            [[
                'name', 'name_en', 'name_kz',
                'title', 'title_en', 'title_kz',
                'url', 'logo', 'image', 'fon_image',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'brand'             => 'Бренд',
            'parent_id'         => 'Родительский категория',
            'name'              => 'Название',
            'name_en'           => 'Название на английском',
            'name_kz'           => 'Название на казахском',
            'url'               => 'Url',
            'logo'              => 'Логотип',
            'image'             => 'Изображение',
            'fon_image'         => 'Фоновое изображение',
            'title'             => 'Заголовок',
            'title_en'          => 'Заголовок на английском',
            'title_kz'          => 'Заголовок на казахском',
            'description'       => 'Описание',
            'description_en'    => 'Описание на английском',
            'description_kz'    => 'Описание на казахском',
            'content'           => 'Контент',
            'content_en'        => 'Контент на английском',
            'content_kz'        => 'Контент на казахском',
            'short_content'     => 'Короткое описание',
            'short_content_en'  => 'Короткое описание на английском',
            'short_content_kz'  => 'Короткое описание на казахском',
            'level'             => 'Уровень',
            'sort'              => 'Сорт',
            'status'            => 'Статус',
            'created_at'        => 'Дата создания',
            'metaName'          => 'Мета Названия',
            'metaName_en'       => 'Мета Названия на английском',
            'metaName_kz'       => 'Мета Названия на казахском',
            'metaDesc'          => 'Мета Описание',
            'metaDesc_en'       => 'Мета Описание на английском',
            'metaDesc_kz'       => 'Мета Описание на казахском',
            'metaKey'           => 'Ключевые слова',
            'metaKey_en'        => 'Ключевые слова на английском',
            'metaKey_kz'        => 'Ключевые слова на казахском',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/category/' . $this->image : '/no-image.png';
    }

    public function getLogo()
    {
        return ($this->logo) ? '/uploads/images/category/' . $this->logo : '/no-image.png';
    }

    public function getFonImage()
    {
        return ($this->fon_image) ? '/uploads/images/category/' . $this->fon_image : '/no-image.png';
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = CategoryInstruments::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParent()
    {
        return $this->hasOne(CategoryInstruments::className(), ['id' => 'parent_id']);
    }

    public function getParentName()
    {
        return (isset($this->parent))? $this->parent->name: ' ';
    }

    public function getChildren()
    {
        return $this->hasMany(CategoryInstruments::className(), ['parent_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->where('level = 1')->all(), 'id', 'name');
    }

    public static function getCategoryList($category_id)
    {
        return self::find()->where(['parent_id' => $category_id])->orderBy('sort DESC')->all();
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    public function getProductOne()
    {
        return $this->hasOne(Products::className(), ['id' => 'category_id']);
    }
}
