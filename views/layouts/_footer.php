<? $copyright = "copyright" . Yii::$app->session["lang"]; ?>
<? $name = "name" . Yii::$app->session["lang"]; ?>

<section class="offer-2">
    <div class="container">
        <div class="title">Записывайтесь
            <br>на визит в шоу рум -
            <br><span>получайте каталог</span></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="book col-md-8">
                <img src="/public/images/Layer 15.png" width="100%" alt="">
            </div>
            <form action="" class="col-md-4">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <input type="text" name="Record[name]" placeholder="ваше имя">
                <input type="text" name="Record[email]" placeholder="электронная почта">
                <input type="text" name="Record[phone]" placeholder="номер телефона">
                <button type="button" class="button record-btn">ЗАПИСАТЬСЯ В ШОУ-РУМ</button>
            </form>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <ul class="col-md-9 p-0">
                <li>
                   <a href="http://worldspianos.kz/"><img src="<?= Yii::$app->view->params['logo'][0]->getImage(); ?>" alt=""></a>
                </li>
                <? foreach (Yii::$app->view->params['menu'] as $item) : ?>
                    <li><a href="<?= $item->url; ?>"><?= $item->$name; ?></a> </li>
                <? endforeach; ?>
                <br><br>
                <div class="link-lux">
                    <a href="https://www.a-lux.kz/" target="blank">Developed by: A-LUX</a>
                </div>
            </ul>
            <div class="col-md-3 p-0">
                <div class="text"><?= Yii::$app->view->params['logo'][0]->$copyright ?></div>
            </div>
        </div>
    </div>
</footer>

</div>