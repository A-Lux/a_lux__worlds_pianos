
<!-- contact -->
<section class="contact-page">
    <div class="container">
        <!-- breadcrumbs -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?=$menu[0]->text;?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?=$menu[4]->text;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <!-- contact title -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="contact-title"><?=$menu[4]->text;?></p>
            </div>
        </div>
        <!-- end contact title -->
        <!-- box contact -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="box-contact">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="contact-list-info">
                            <div class="col-sm-12 col-md-12 offset-lg-1 col-lg-11">
                                <ul class="contact-location">
                                    <li class="contact-location-title">
                                        Адрес:
                                    </li>
                                    <li class="contact-location-info">
                                        <?=$contact->address;?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-6 offset-lg-1 col-lg-5">
                                <ul class="contact-phone">
                                    <li class="contact-phone-title">
                                        Телефон:
                                    </li>
                                    <?foreach($phone as $value):?>
                                        <li class="contact-phone-info">
                                            <?=$value->name;?>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-6 offset-lg-1 col-lg-5">
                                <ul class="contact-clock">
                                    <li class="contact-clock-title">
                                        График работы:
                                    </li>
                                    <li class="contact-clock-info">
                                        <?=$contact->work_time;?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-12 offset-lg-1 col-lg-5">
                                <ul class="contact-mail">
                                    <li class="contact-mail-title">
                                        Почта:
                                    </li>
                                    <li class="contact-mail-info">
                                        <a href="mailto:<?= $contact->email; ?>"><?=$contact->email;?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="map">
                            <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3" class="contact-map">
                                <div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div>
                                <script>
                                    (function () {
                                        var setting = {
                                            "height": 297,
                                            "width": 500,
                                            "zoom": 17,
                                            "queryString": "БЦ \"Алатау\", улица Муканова, Алматы, Казахстан",
                                            "place_id": "ChIJuZDWgEppgzgRpKR1bPmuzU4",
                                            "satellite": false,
                                            "centerCoord": [43.24804608979425, 76.9157926574096],
                                            "cid": "0x4ecdaef96c75a4a4",
                                            "cityUrl": "/kazakhstan/almaty-10297",
                                            "id": "map-9cd199b9cc5410cd3b1ad21cab2e54d3",
                                            "embed_id": "55538"
                                        };
                                        var d = document;
                                        var s = d.createElement('script');
                                        s.src = 'https://1map.com/js/script-for-user.js?embed_id=55538';
                                        s.async = true;
                                        s.onload = function (e) {
                                            window.OneMap.initMap(setting)
                                        };
                                        var to = d.getElementsByTagName('script')[0];
                                        to.parentNode.insertBefore(s, to);
                                    })();
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end box contact -->
    </div>
</section>
