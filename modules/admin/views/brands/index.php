<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brands-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Brands', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'name_en',
            'name_kz',
            'url:url',
            //'logo',
            //'image',
            //'fon_image',
            //'title',
            //'title_en',
            //'title_kz',
            //'description:ntext',
            //'description_en:ntext',
            //'description_kz:ntext',
            //'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',
            //'sort',
            //'status',
            //'created_at',
            //'metaName',
            //'metaDesc:ntext',
            //'metaKey:ntext',
            //'metaName_en',
            //'metaName_kz',
            //'metaKey_en:ntext',
            //'metaKey_kz:ntext',
            //'metaDesc_en:ntext',
            //'metaDesc_kz:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
