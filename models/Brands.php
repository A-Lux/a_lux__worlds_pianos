<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $url
 * @property string $logo
 * @property string $image
 * @property string $fon_image
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaKey_en
 * @property string $metaKey_kz
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'sort', 'status'], 'required'],
            [[
                'description',  'description_en',   'description_kz',
                'content',      'content_en',       'content_kz',
                'metaDesc',     'metaDesc_en',      'metaDesc_kz',
                'metaKey',      'metaKey_en',       'metaKey_kz'
            ], 'string'],
            [['sort', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [[
                'name', 'name_en', 'name_kz',
                'title', 'title_en', 'title_kz',
                'url', 'logo', 'image', 'fon_image',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'brand'         => 'Бренд',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'url'           => 'Url',
            'logo'          => 'Логотип',
            'image'         => 'Изображение',
            'fon_image'     => 'Фоновое изображение',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок на английском',
            'title_kz'      => 'Заголовок на казахском',
            'description'   => 'Описание',
            'description_en'=> 'Описание на английском',
            'description_kz'=> 'Описание на казахском',
            'content'       => 'Контент',
            'content_en'    => 'Контент на английском',
            'content_kz'    => 'Контент на казахском',
            'sort'          => 'Сорт',
            'status'        => 'Статус',
            'created_at'    => 'Дата создания',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия на английском',
            'metaName_kz'   => 'Мета Названия на казахском',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание на английском',
            'metaDesc_kz'   => 'Мета Описание на казахском',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова на английском',
            'metaKey_kz'    => 'Ключевые слова на казахском',
        ];
    }
}
