<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property int $menu_id
 * @property string $image
 * @property string $note
 * @property int $sort
 */
class Images extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/images/';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id'], 'required'],
            [['menu_id', 'sort'], 'integer'],
            [['image', 'note'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Страница изображения',
            'image' => 'Изображение',
            'note' => 'Заметка',
            'sort' => 'Сорт',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = Images::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/images/' . $this->image : '/no-image.png';
    }

    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    public function getMenuName()
    {
        return (isset($this->menu))? $this->menu->name: ' ';
    }
}
