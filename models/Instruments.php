<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "instruments".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $color
 * @property string $color_en
 * @property string $color_kz
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class Instruments extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/instruments/';

    public $catalog;
    public $brand;
    public $instruments;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instruments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'content', 'color'], 'required'],
            [['category_id'], 'integer'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['name', 'name_en', 'name_kz', 'image', 'color', 'color_en', 'color_kz',
                'metaName', 'metaName_en', 'metaName_kz'], 'string', 'max' => 255],
            [['metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Название',
            'name_en' => 'Название на английском',
            'name_kz' => 'Название на казахском',
            'image' => 'Изображение',
            'content' => 'Содержание',
            'content_en' => 'Содержание на английском',
            'content_kz' => 'Содержание на казахском',
            'color' => 'Цвет инструмента',
            'color_en' => 'Цвет инструмента на английском',
            'color_kz' => 'Цвет инструмента на казахском',
            'metaName' => 'Мета Названия',
            'metaName_en' => 'Мета Названия на английском',
            'metaName_kz' => 'Мета Названия на казахском',
            'metaDesc' => 'Мета Описание',
            'metaDesc_en' => 'Мета Описание на английском',
            'metaDesc_kz' => 'Мета Описание на казахском',
            'metaKey' => 'Ключевые слова',
            'metaKey_en' => 'Ключевые слова на английском',
            'metaKey_kz' => 'Ключевые слова на казахском',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/instruments/' . $this->image : '/no-image.png';
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getCategory()
    {
        return $this->hasOne(CategoryInstruments::className(), ['id' => 'category_id']);
    }

    public function getCategoryName(){
        return (isset($this->category))? $this->category->name:'Не задан';
    }

}
