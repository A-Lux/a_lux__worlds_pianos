<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Banner;
use app\models\search\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post())) {
            $image              = UploadedFile::getInstance($model, 'image');
            $image_background   = UploadedFile::getInstance($model, 'image_background');
            $video              = UploadedFile::getInstance($model, 'video');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            if($image_background != null) {
                $time = time();
                $image_background->saveAs($model->path . $time . '_' . $image_background->baseName . '.' . $image_background->extension);
                $model->image_background = $time . '_' . $image_background->baseName . '.' . $image_background->extension;
            }

            if($video != null) {
                $time = time();
                $video->saveAs($model->pathVideo . $time . '_' . $video->baseName . '.' . $video->extension);
                $model->video = $time . '_' . $video->baseName . '.' . $video->extension;
            }

            if($model->save(false)){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model              = $this->findModel($id);
        $oldImage           = $model->image;
        $oldImageBackground = $model->image_background;
        $oldVideo           = $model->video;

        if ($model->load(Yii::$app->request->post())) {
            $image              = UploadedFile::getInstance($model, 'image');
            $image_background   = UploadedFile::getInstance($model, 'image_background');
            $video              = UploadedFile::getInstance($model, 'video');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    if(file_exists($model->path.$oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }

            if($image_background == null){
                $model->image_background = $oldImageBackground;
            }else{
                $time = time();
                $image_background->saveAs($model->path . $time . '_' . $image_background->baseName . '.' . $image_background->extension);
                $model->image_background = $time . '_' . $image_background->baseName . '.' . $image_background->extension;
                if(!($oldImageBackground == null)){
                    if(file_exists($model->path.$oldImageBackground)) {
                        unlink($model->path . $oldImageBackground);
                    }
                }
            }

            if($video == null){
                $model->video = $oldVideo;
            }else{
                $time = time();
                $video->saveAs($model->pathVideo . $time . '_' . $video->baseName . '.' . $video->extension);
                $model->video = $time . '_' . $video->baseName . '.' . $video->extension;
                if(!($oldVideo == null)){
                    if(file_exists($model->pathVideo.$oldVideo)) {
                        unlink($model->pathVideo . $oldVideo);
                    }
                }
            }

            if($model->save(false)){
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImg($id)
    {
        $model = Banner::findOne($id);

        $model->image = null;

        $model->save(false);

        return $this->redirect(['update?id='.$id]);
    }

    public function actionDeleteImageBackground($id)
    {
        $model = Banner::findOne($id);

        $model->image_background = null;

        $model->save(false);

        return $this->redirect(['update?id='.$id]);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
