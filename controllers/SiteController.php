<?php

namespace app\controllers;

use app\models\About;
use app\models\Artists;
use app\models\Banner;
use app\models\Catalog;
use app\models\CategoryInstruments;
use app\models\Images;
use app\models\Logo;
use app\models\MainSub;
use app\models\Contact;
use app\models\News;
use app\models\Menu;
use app\models\Record;
use app\models\Request;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


class SiteController extends FrontendController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@app/web/fonts/Century Gothic.ttf',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Menu::find()->where('url = "/"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $mainSub = MainSub::find()->all();
        $logo = Logo::find()->orderBy('id DESC')->one();
        $banner = Banner::find()->where(['menu_id' => $model->id])->all();
        $instruments = CategoryInstruments::find()->where(['level' => 1])->all();
        $artists = Artists::find()->orderBy('id ASC')->all();
        $images = Images::find()->where(['menu_id' => $model->id])->orderBy('id ASC')->one();

        return $this->render('index', compact('model', 'banner', 'mainSub',
            'instruments', 'artists', 'logo', 'images'));
    }


    public function actionRecord()
    {
        $model = new Record();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    return 1;
                }
            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionRequest()
    {
        $model = new Request();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    return 1;
                }
            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

}
