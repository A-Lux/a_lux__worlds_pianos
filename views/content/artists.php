<? $name = "name".Yii::$app->session["lang"];?>
<? $text = "text".Yii::$app->session["lang"];?>
<div class="artist-bread">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item asdasd"><a href="/"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$model->$name;?>
                </li>
            </ol>
        </nav>
    </div>
</div>

<div class="main artists-main">
    <div class="container page-title"><?=$model->$name;?></div>
    <? foreach($category as $item) :?>
        <? if(!empty($item->children)): ?>
            <div class="subtitle-gallery container"><?=$item->$name;?></div>
            <div class="gallery gallery-solo container p-1">

                <? foreach($item->children as $value): ?>
                    <div class="item p-0 m-1 col-sm-12 col-md-6 col-xl-4">
                    <a href="<?=\yii\helpers\Url::to(['/content/card-artist', 'id' => $value->id])?>">
                    <div class="img">
                        <img src="<?=$value->getImage();?>" alt="">
                    </div>
                    </a>
                    <div class="text">
                        <div class="name"><?=$value->$name;?></div>
                        <a href="<?=\yii\helpers\Url::to(['/content/card-artist', 'id' => $value->id])?>" class="button">ПОДРОБНЕЕ</a>
                    </div>
                </div>
                <?endforeach;?>

            </div>
        <?endif; ?>
    <?endforeach;?>

</div>
