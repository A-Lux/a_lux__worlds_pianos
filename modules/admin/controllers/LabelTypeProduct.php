<?php

namespace app\modules\admin\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelTypeProduct
{
    public static function typeList()
    {
        return [
            0 => 'Рояль',
            1 => 'Пианино',
        ];
    }

    public static function typeLabel($type)
    {
        switch ($type) {
            case 0:
                $class = 'label label-success';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-success';
        }

        return Html::tag('span', ArrayHelper::getValue(self::typeList(), $type), [
            'class' => $class,
        ]);
    }
}