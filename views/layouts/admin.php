<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?=Yii::$app->user->identity->username?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">

                                <p>
                                    <?=Yii::$app->user->identity->username?> - Administrator
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=Yii::$app->user->identity->username?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/admin/menu/index'],'active' => $this->context->id == 'menu'],
                        ['label' => 'Боковое меню', 'icon' => 'bookmark-o', 'url' => ['/admin/sidebar/index'],'active' => $this->context->id == 'sidebar'],

                        [
                            'label' => 'Страницы',
                            'icon' => 'book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Заголовки контента', 'icon' => 'book', 'url' => ['/admin/main-sub/index'],  'active' => $this->context->id == 'main-sub'],
                                ['label' => 'Логотип и копирайт', 'icon' => 'edit', 'url' => ['/admin/logo/index'],  'active' => $this->context->id == 'logo'],
                                ['label' => 'Баннеры', 'icon' => 'columns', 'url' => ['/admin/banner/index'],  'active' => $this->context->id == 'banner'],
                                ['label' => 'Spirio', 'icon' => 'gavel', 'url' => ['/admin/spirio/index'], 'active' => $this->context->id == 'spirio'],
                                ['label' => 'Инструменты', 'icon' => 'briefcase', 'url' => ['/admin/page-instrument/index'], 'active' => $this->context->id == 'page-instrument'],
                                ['label' => 'Инвестиции', 'icon' => 'money', 'url' => ['/admin/investments/index'], 'active' => $this->context->id == 'investments'],
                                ['label' => 'Путеводитель покупателя', 'icon' => 'map', 'url' => ['/admin/guide/index'], 'active' => $this->context->id == 'guide'],
                                ['label' => 'Шоу рум', 'icon' => 'map', 'url' => ['/admin/shourum/index'], 'active' => $this->context->id == 'shourum'],
                            ],
                        ],
                        ['label' => 'Изображении', 'icon' => 'image', 'url' => ['/admin/images/index'],'active' => $this->context->id == 'images'],
                        [
                            'label' => 'Цвета',
                            'icon' => 'image',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Цвета', 'icon' => 'image', 'url' => ['/admin/colors/index'],'active' => $this->context->id == 'colors'],
                                ['label' => 'Цвета продуктов', 'icon' => 'image', 'url' => ['/admin/instrument-color/index'],'active' => $this->context->id == 'instrument-color'],
                            ],
                        ],
                        [
                            'label' => 'Инструменты',
                            'icon' => 'envira',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Баннеры брендов', 'icon' => 'columns', 'url' => ['/admin/banner-brand/index'],'active' => $this->context->id == 'banner-brand'],
                                ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/category-instruments/index'],'active' => $this->context->id == 'category-instruments'],
                                ['label' => 'Инструменты', 'icon' => 'fa fa-user', 'url' => ['/admin/products/index'], 'active' => $this->context->id == 'products'],
                            ],
                        ],
                        [
                            'label' => 'Артисты',
                            'icon' => 'users',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/category-artists/index'],'active' => $this->context->id == 'category-artists'],
                                ['label' => 'Артисты', 'icon' => 'user', 'url' => ['/admin/artists/index'],  'active' => $this->context->id == 'artists'],
                            ],
                        ],
                        [
                            'label' => 'Контакты',
                            'icon' => 'address-book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Контакты', 'icon' => 'street-view', 'url' => ['/admin/contact/index'],  'active' => $this->context->id == 'contact'],
                                ['label' => 'Эл. почта для связи', 'icon' => 'at', 'url' => ['/admin/emailforrequest/index'],'active' => $this->context->id == 'emailforrequest'],
                            ],
                        ],
                        [
                            'label' => 'Новости',
                            'icon' => 'calendar',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Новости', 'icon' => 'calendar', 'url' => ['/admin/news/index'],'active' => $this->context->id == 'news'],
                                ['label' => 'Изображении', 'icon' => 'image', 'url' => ['/admin/news-images/index'],'active' => $this->context->id == 'news-images'],
                            ],
                        ],
                        [
                            'label' => 'Заявки',
                            'icon' => 'address-book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Заявки к визиту', 'icon' => 'ticket', 'url' => ['/admin/record/index'],'active' => $this->context->id == 'record'],
                                ['label' => 'Заявки по вопросам', 'icon' => 'question', 'url' => ['/admin/request/index'],  'active' => $this->context->id == 'request'],
                            ],
                        ],
                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b></b>
        </div>
        <strong>Copyright &copy; 2007-2019 <a href="https://a-lux.kz">A-lux</a>.</strong>
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/private/bower_components/ckeditor/ckeditor.js');?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    });
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
