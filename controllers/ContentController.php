<?php

namespace app\controllers;

use app\models\About;
use app\models\Artists;
use app\models\Banner;
use app\models\CategoryArtists;
use app\models\Contact;
use app\models\Images;
use app\models\Investments;
use app\models\MainSub;
use app\models\Menu;
use app\models\News;
use app\models\NewsImages;
use app\models\Shourum;
use app\models\Spirio;
use Yii;

class ContentController extends FrontendController
{
    public $layout = "main";

    public function actionSpirio()
    {
        $model = Menu::find()->where('url = "/content/spirio"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $banner = Banner::find()->where(['menu_id' => $model->id])->all();
        $spirio = Spirio::find()->orderBy('id ASC')->one();
        $images = Images::find()->where(['menu_id' => $model->id])->orderBy('sort ASC')->all();

        return $this->render('spirio', compact('model', 'spirio', 'images',
            'banner'));
    }

    public function actionInvestments()
    {
        $model = Menu::find()->where('url = "/content/investments"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $banner = Banner::find()->where(['menu_id' => $model->id])->orderBy('id DESC')->one();
        $investments = Investments::find()->orderBy('id ASC')->one();
        $contact = Contact::find()->orderBy('id ASC')->one();

        return $this->render('investments', compact('model', 'investments', 'banner', 'contact'));
    }

    public function actionNews()
    {
        $model = Menu::find()->where('url = "/content/news"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $news = News::getAll(6);

        return $this->render('news', compact('model', 'news'));
    }

    public function actionNewsCard($id)
    {
        $news = News::findOne(['id' => $id]);
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($news->$metaName, $news->$metaKey, $news->$metaDesc);

        $images = NewsImages::find()->where(['news_id' => $news->id])->all();

        return $this->render('news-card', compact('news', 'images'));
    }

    public function actionShourum()
    {
        $model = Menu::find()->where('url = "/content/shourum"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $shourum = Shourum::find()->orderBy('id ASC')->one();

        $time = Yii::$app->formatter->asTimestamp($shourum->date);

        $time = Yii::$app->formatter->asDate($time, 'php: m.d.Y H:i:s');

        return $this->render('shourum', compact('model', 'time'));
    }

    public function actionArtists()
    {
        $model = Menu::find()->where('url = "/content/artists"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $category = CategoryArtists::find()->all();
        $artists = Artists::getAll(6, $category->id);

        return $this->render('artists', compact('model', 'category', 'artists'));
    }

    public function actionCardArtist($id)
    {
        $artist = Artists::findOne(['id' => $id]);
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($artist->$metaName, $artist->$metaKey, $artist->$metaDesc);

        return $this->render('artist-card', compact('artist'));
    }

    public static function cutStr($str, $length=50, $postfix=' ...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }

}