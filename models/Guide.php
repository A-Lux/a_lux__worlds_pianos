<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guide".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $short_content
 * @property string $short_content_en
 * @property string $short_content_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $image
 * @property string $background_image
 * @property string $file
 */
class Guide extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/guide/';
    public $pathFile = 'uploads/files/guide/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guide';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'subtitle', 'description'], 'required'],
            [[
                'content', 'content_en', 'content_kz',
                'description', 'description_en', 'description_kz',
                'short_content', 'short_content_en', 'short_content_kz',
            ], 'string'],
            [[
                'title', 'title_en', 'title_kz',
                'name', 'name_en', 'name_kz',
                'subtitle', 'subtitle_en', 'subtitle_kz',
                'image', 'background_image', 'file'
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Заголовок',
            'title_en'          => 'Заголовок на английском',
            'title_kz'          => 'Заголовок на казахском',
            'name'              => 'Название на казахском',
            'name_en'           => 'Название на казахском',
            'name_kz'           => 'Название на казахском',
            'short_content'     => 'Короткое описание',
            'short_content_en'  => 'Короткое описание на английском',
            'short_content_kz'  => 'Короткое описание на казахском',
            'content'           => 'Описание',
            'content_en'        => 'Описание на английском',
            'content_kz'        => 'Описание на казахском',
            'subtitle'          => 'Подзаголовок',
            'subtitle_en'       => 'Подзаголовок на английском',
            'subtitle_kz'       => 'Подзаголовок на казахском',
            'description'       => 'Второе описание',
            'description_en'    => 'Второе описание на английском',
            'description_kz'    => 'Второе описание на казахском',
            'image'             => 'Изображение',
            'background_image'  => 'Фоновое изображение',
            'file'              => 'Файл',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/guide/' . $this->image : '/no-image.png';
    }

    public function getBackImage()
    {
        return ($this->background_image) ? '/uploads/images/guide/' . $this->background_image : '/no-image.png';
    }

    public function getFilePath()
    {
        return ($this->file) ? '/uploads/files/guide/' . $this->file : '';
    }
}
