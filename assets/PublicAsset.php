<?php

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'public/css/canvi.css',
        'public/css/bootstrap.css',
        'public/libs/slick/slick.css',
        'public/libs/slick/slick-theme.css',
        'public/css/main.css',

    ];
    public $js = [
        "public/js/bootstrap.js",
        "public/js/canvi.js",
        "public/libs/slick/slick.js",
        "public/js/main.js",
        "public/js/record.js",

    ];
    public $depends = [

    ];
}