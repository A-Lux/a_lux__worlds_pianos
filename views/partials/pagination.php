<?php

use yii\widgets\LinkPager;

?>
    <div class="pag">
        <div class="strip"></div>
        <ul class="pagination">
        <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png" alt=""></a></li>
            <?php
            echo LinkPager::widget([
                'pagination' => $pages,
                'pageCssClass' => ['class' => 'page-item'],
                'options' => ['class' => 'pagination'],
                'activePageCssClass' => 'active',
                'linkOptions' => ['class' => 'page-link'],
                'hideOnSinglePage' => true,
                'prevPageLabel' => '<img src="/public/images/right-arrow (1).png" alt="">',
                'prevPageCssClass' => ['class' => 'page-item page-link'],
                'nextPageLabel' => '<img src="/public/images/right-arrow (1).png" alt="">',
                'nextPageCssClass' => ['class' => 'page-item page-link'],
            ]);
            ?>

          
<!--            <li class="page-item"><a class="page-link active" href="#">1</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">4</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="#">5</a></li>-->
           <li class="page-item"><a class="page-link" href="#"><img src="/public/images/right-arrow (1).png" alt=""></a></li>
        </ul>
        <div class="strip"></div>
    </div>
