<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.04.2019
 * Time: 11:45
 */

namespace app\controllers;

use app\models\Logo;
use app\models\Sidebar;
use app\models\Telephone;
use app\models\Catalog;
use app\models\CategoryInstruments;
use app\models\Contact;
use app\models\Emailforrequest;
use app\models\Feedback;
use app\models\Menu;
use app\models\User;
use app\models\Products;
use app\models\SignupForm;
use app\models\UserProfile;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {
        $main = Menu::find()->where('url = "/"')->one();
        $name = "name".Yii::$app->session["lang"];

        $menu = Menu::find()->where(['status' => 1])->orderBy('sort asc')->limit(7)->all();
        $logo = Logo::find()->all();
        $contact = Contact::find()->orderBy('id ASC')->one();
        $sidebar = Sidebar::find()->orderBy('sort DESC')->all();

        Yii::$app->view->params['main'] = $main->$name;
        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['logo'] = $logo;
        Yii::$app->view->params['contact'] = $contact;
        Yii::$app->view->params['sidebar'] = $sidebar;

    }

    protected function setMeta($metaName = null, $metaKey = null, $metaDesc = null)
    {
        $this->view->title = $metaName;
        $this->view->registerMetaTag(['name' => 'metaKey', 'content' => "$metaKey"]);
        $this->view->registerMetaTag(['name' => 'metaDesc', 'content' => "$metaDesc"]);
    }

}