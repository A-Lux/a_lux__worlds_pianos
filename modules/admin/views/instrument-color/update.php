<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InstrumentColor */

$this->title = 'Редактирование цвет инструмента: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Цвета инструментов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="instrument-color-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
