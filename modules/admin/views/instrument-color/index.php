<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\InstrumentColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цвета инструментов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrument-color-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить цвет инструмента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'color_id',
                'filter' => \app\models\Colors::getList() ,
                'value' => function ($model) {
                    return Html::img($model->color->getImage(), ['width'=>100]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'product_id',
                'filter' => \app\models\Products::getList(),
                'value' => function ($model) {
                    return Html::a($model->products->name, ['view', 'id' => $model->products->id]);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
