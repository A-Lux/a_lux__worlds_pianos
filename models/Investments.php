<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "investments".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $image
 */
class Investments extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/investments/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'investments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'subtitle', 'description'], 'required'],
            [['content', 'content_en', 'content_kz', 'description', 'description_en', 'description_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'subtitle', 'subtitle_en', 'subtitle_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'title_en' => 'Заголовок на английском',
            'title_kz' => 'Заголовок на казахском',
            'content' => 'Описание',
            'content_en' => 'Описание на английском',
            'content_kz' => 'Описание на казахском',
            'subtitle' => 'Подзаголовок',
            'subtitle_en' => 'Подзаголовок на английском',
            'subtitle_kz' => 'Подзаголовок на казахском',
            'description' => 'Второе описание',
            'description_en' => 'Второе описание на английском',
            'description_kz' => 'Второе описание на казахском',
            'image' => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/investments/' . $this->image : '';
    }
}
