<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<? $subtitle = "subtitle" . Yii::$app->session["lang"]; ?>
<? $description = "description" . Yii::$app->session["lang"]; ?>
<? $short_content = "short_content" . Yii::$app->session["lang"]; ?>
<? $content = "content" . Yii::$app->session["lang"]; ?>
<? $text = "text" . Yii::$app->session["lang"]; ?>
<?php

use yii\helpers\Url;

?>
<div class="instruments-bread">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?= Yii::$app->view->params['main']; ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?= $model->$name; ?></li>
            </ol>
        </nav>
    </div>
</div>

<div class="instruments-main">
    <div class="container page-title"><?=$page_instruments->$title?></div>

    <div class="article">
        <div class="container">
            <div class="text width-text">
                <?=$page_instruments->$description;?>
            </div>
        </div>
    </div>

    <div class="instruments-gallery">
        <div class="row">
            <div class="col-12 p-1">
                <div class="instrument">
                    <a href="<?= Url::to(['/catalog/brand', 'url' => $instruments[0]->url]) ?>" class="title">
                        <div class="img">
                            <img src="<?= $instruments[0]->getFonImage(); ?>" alt="">
                        </div>
                        <div class="text">
                            <?= $instruments[0]->$name; ?>
                            <p><?=$instruments[0]->$short_content?></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 p-1">
                <div class="instrument">
                    <a href="<?= Url::to(['/catalog/brand', 'url' => $instruments[1]->url]) ?>" class="title">
                        <div class="img">
                            <img src="<?= $instruments[1]->getFonImage(); ?>" alt="">
                        </div>
                        <div class="text">
                            <?= $instruments[1]->$name; ?>
                            <p><?=$instruments[1]->$short_content?></p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 p-1">
                <div class="instrument">
                    <a href="<?= Url::to(['/catalog/brand', 'url' => $instruments[2]->url]) ?>" class="title">
                        <div class="img">
                            <img src="<?= $instruments[2]->getFonImage(); ?>" alt="">
                        </div>
                        <div class="text">
                            <?= $instruments[2]->$name; ?>
                            <p><?=$instruments[2]->$short_content?></p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 p-1">
                <div class="instrument">
                    <a href="<?=Url::to(['/catalog/guide'])?>" class="title">
                        <div class="img">
                            <img src="<?=$guide->getBackImage();?>" alt="">
                        </div>
                        <div class="text">
                            <?=$guide->$name;?>
                            <p><?=$guide->$short_content?></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container article-title"><?=$page_instruments->$subtitle;?></div>

    <div class="article">
        <div class="container">
            <div class="text width-text">
               <?=$page_instruments->$content;?>
            </div>
        </div>
    </div>

    <div class="photo-fon">
        <img src="<?=$images->getImage();?>" width="100%" alt="">
    </div>

    <div class="container blockquote-comment">
        <blockquote>
            <?=$page_instruments->$text;?>
        </blockquote>
    </div>
</div>
