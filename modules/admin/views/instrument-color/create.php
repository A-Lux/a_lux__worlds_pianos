<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InstrumentColor */

$this->title = 'Добавить цвет инструмента';
$this->params['breadcrumbs'][] = ['label' => 'Цвета инструментов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrument-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
