<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Guide */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Путеводитель покупателя', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="guide-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?php $files = $model->getFilePath(); ?>
    <?php if($files != null) $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else $file =  ['attribute'=>'file','value' => '']?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'title_kz',
            'name',
            'name_en',
            'name_kz',
            'short_content:ntext',
            'short_content_en:ntext',
            'short_content_kz:ntext',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            'subtitle',
            'subtitle_en',
            'subtitle_kz',
            'description:ntext',
            'description_en:ntext',
            'description_kz:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>300]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'background_image',
                'value' => function($data){
                    return Html::img($data->getBackImage(), ['width'=>300]);
                }
            ],
            $file,
        ],
    ]) ?>

</div>
