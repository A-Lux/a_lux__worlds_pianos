<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "artists".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 * @property string $url
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class Artists extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/artists/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'name', 'content', 'category_id'], 'required'],
            [['title', 'title_en', 'title_kz', 'content', 'content_en', 'content_kz'], 'string'],
            [['name', 'name_en', 'name_kz', 'image', 'url',
                'metaName', 'metaName_en', 'metaName_kz'], 'string', 'max' => 255],
            [['metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'title' => 'Заголовок',
            'title_en' => 'Заголовок на английском',
            'title_kz' => 'Заголовок на казахском',
            'name' => 'Название',
            'name_en' => 'Название на английском',
            'name_kz' => 'Название на казахском',
            'image' => 'Изображение',
            'url' => 'Url',
            'content' => 'Содержание',
            'content_en' => 'Содержание на английском',
            'content_kz' => 'Содержание на казахском',
            'metaName' => 'Мета Названия',
            'metaName_en' => 'Мета Названия на английском',
            'metaName_kz' => 'Мета Названия на казахском',
            'metaDesc' => 'Мета Описание',
            'metaDesc_en' => 'Мета Описание на английском',
            'metaDesc_kz' => 'Мета Описание на казахском',
            'metaKey' => 'Ключевые слова',
            'metaKey_en' => 'Ключевые слова на английском',
            'metaKey_kz' => 'Ключевые слова на казахском',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/artists/' . $this->image : '/no-image.png';
    }

    public function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getCategory()
    {
        return $this->hasOne(CategoryArtists::className(), ['id' => 'category_id']);
    }

    public function getCategoryName()
    {
        return (isset($this->category))? $this->category->name:'Не задан';
    }

    public static function getAll($pageSize, $category_id)
    {
        $query = Artists::find()->where(['category_id' => $category_id])->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
