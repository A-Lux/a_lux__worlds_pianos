$('input[name="Record[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="Request[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="Request[phone]"]').inputmask("8(999) 999-9999");


$('body').on('click', ".record-btn", function (e) {

    $.ajax({
        type: "POST",
        url: "/site/record",
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if (data == 1) {
                swal('Заявка отправлена');

            } else {
                swal('Ошибка', data, 'error');
            }
        },
        error: function () {
            swal('Error', 'error');
        }
    });
});

$('body').on('click', ".request-btn", function (e) {

    $.ajax({
        type: "POST",
        url: "/site/request",
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if (data == 1) {
                swal('Заявка отправлена');

            } else {
                swal('Ошибка', data, 'error');
            }
        },
        error: function () {
            swal('Error', 'error');
        }
    });
});
