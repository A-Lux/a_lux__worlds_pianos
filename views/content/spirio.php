<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<? $content = "content" . Yii::$app->session["lang"]; ?>
<? $subtitle = "subtitle" . Yii::$app->session["lang"]; ?>
<? $text = "text" . Yii::$app->session["lang"]; ?>
<div class="spirio-main">

    <div class="first-block">

        <div class="slider-first">
            <? foreach ($banner as $item) : ?>
                <? if ($item->status == 0) : ?>
                    <div class="slider-item" style="background-image:url('<?= $item->getImageBackground(); ?>');">
                        <div class="container">
                            <div class="title"><?= $item->$title ?></div>
                        </div>
                    </div>
                <? elseif ($item->status == 1) : ?>
                    <video src="<?= $item->getVideo(); ?>" controls autoplay="true"></video>
                <? endif; ?>
            <? endforeach; ?>

        </div>
    </div>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><?= Yii::$app->view->params['main']; ?></a></li>
                <li class="breadcrumb-item" aria-current="page"><?= $model->$name; ?></li>
            </ol>
        </nav>
    </div>


    <div class="container page-title">
        <div class="page-title"><?= $spirio->$title; ?></div>
    </div>


    <div class="article">
        <div class="container">
            <div class="text">
                <?= $spirio->$content; ?>
            </div>
            <a href="#" data-toggle="modal" data-target="#myModal" class="button">Узнать цену</a>
        </div>
    </div>

    <div class="gallery-photo">
        <div class="row m-0">
            <?php for ($i = 0; $i < 2; $i++) { ?>
                <?php if (!isset($images[$i])) continue; ?>
                <div class="gallery-photo-item col-md-<?= $i % 2 == 0 ? '8' : '4' ?> p-1">
                    <img src="<?= $images[$i]->getImage() ?>" alt="">
                </div>
            <?php } ?>
        </div>
        <div class="row m-0">
            <?php for ($i = 2; $i < 4; $i++) { ?>
                <?php if (!isset($images[$i])) continue; ?>
                <div class="gallery-photo-item col-md-<?= $i % 2 == 0 ? '4' : '8' ?> p-1">
                    <img src="<?= $images[$i]->getImage() ?>" alt="">
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="container page-title">
        <div class="page-title"><?= $spirio->$subtitle; ?></div>
    </div>

    <div class="article">
        <div class="container">
            <div class="text">
                <?= $spirio->$text; ?>
            </div>
        </div>
    </div>

    <div class="photo-fon"></div>


</div>