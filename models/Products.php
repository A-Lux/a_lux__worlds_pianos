<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $model
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 * @property string $background_image
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $height
 * @property string $width
 * @property string $depth
 * @property string $weight
 * @property string $length
 * @property string $type
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class Products extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/products/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id'], 'integer'],
            [[
                'description', 'description_en', 'description_kz',
                'content', 'content_en', 'content_kz',
                'metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'
            ], 'string'],
            [[
                'name', 'name_en', 'name_kz',
                'image', 'type', 'model', 'background_image',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],

            [['height', 'width', 'depth', 'weight', 'length'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'category_id'       => 'Бренд',
            'model'             => 'Модель',
            'name'              => 'Название',
            'name_en'           => 'Название на английском',
            'name_kz'           => 'Название на казахском',
            'image'             => 'Изображение',
            'background_image'  => 'Фоновое изображение',
            'description'       => 'Описание',
            'description_en'    => 'Описание на английском',
            'description_kz'    => 'Описание на казахском',
            'content'           => 'Содержание',
            'content_en'        => 'Содержание на английском',
            'content_kz'        => 'Содержание на казахском',
            'height'            => 'Высота',
            'width'             => 'Ширина',
            'depth'             => 'Глубина',
            'weight'            => 'Вес',
            'length'            => 'Длина',
            'type'              => 'Тип',
            'metaName'          => 'Мета Названия',
            'metaName_en'       => 'Мета Названия на английском',
            'metaName_kz'       => 'Мета Названия на казахском',
            'metaDesc'          => 'Мета Описание',
            'metaDesc_en'       => 'Мета Описание на английском',
            'metaDesc_kz'       => 'Мета Описание на казахском',
            'metaKey'           => 'Ключевые слова',
            'metaKey_en'        => 'Ключевые слова на английском',
            'metaKey_kz'        => 'Ключевые слова на казахском',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/products/' . $this->image : '/no-image.png';
    }

    public function getBackImage()
    {
        return ($this->background_image) ? '/uploads/images/products/' . $this->background_image : '/no-image.png';
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getCategory()
    {
        return $this->hasOne(CategoryInstruments::className(), ['id' => 'category_id']);
    }

    public function getCategoryName()
    {
        return (isset($this->category))? $this->category->name:'Не задан';
    }

    public function getInstrumentColor()
    {
        return $this->hasMany(InstrumentColor::className(), ['product_id' => 'id']);
    }
}
