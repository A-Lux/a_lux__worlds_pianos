<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Shourum */

$this->title = 'Открытие шоу рума';
$this->params['breadcrumbs'][] = ['label' => 'Шоу рум', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shourum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
