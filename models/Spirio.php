<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spirio".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $image
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 */
class Spirio extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/spirio/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spirio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'subtitle', 'text'], 'required'],
            [['content', 'content_en', 'content_kz', 'text', 'text_en', 'text_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'subtitle', 'subtitle_en', 'subtitle_kz', 'image', 'name'
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'title' => 'Заголовок',
            'title_en' => 'Заголовок на английском',
            'title_kz' => 'Заголовок на казахском',
            'content' => 'Описание',
            'content_en' => 'Описание на английском',
            'content_kz' => 'Описание на казахском',
            'image' => 'Изображение',
            'subtitle' => 'Подзаголовок',
            'subtitle_en' => 'Подзаголовок на английском',
            'subtitle_kz' => 'Подзаголовок на казахском',
            'text' => 'Второе описание',
            'text_en' => 'Второе описание на английском',
            'text_kz' => 'Второе описание на казахском',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/spirio/' . $this->image : '/no-image.png';
    }
}
