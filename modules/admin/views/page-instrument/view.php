<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PageInstrument */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страница инструменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-instrument-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'title_en',
            'title_kz',
            'description:ntext',
            'description_en:ntext',
            'description_kz:ntext',
            'subtitle',
            'subtitle_en',
            'subtitle_kz',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            'text:ntext',
            'text_en:ntext',
            'text_kz:ntext',
        ],
    ]) ?>

</div>
