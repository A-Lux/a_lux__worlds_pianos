<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_instrument".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $subtitle
 * @property string $subtitle_en
 * @property string $subtitle_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 */
class PageInstrument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_instrument';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['description', 'description_en', 'description_kz', 'content', 'content_en', 'content_kz', 'text', 'text_en', 'text_kz'], 'string'],
            [['title', 'title_en', 'title_kz', 'subtitle', 'subtitle_en', 'subtitle_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Заголовок',
            'title_en'          => 'Заголовок на английском',
            'title_kz'          => 'Заголовок на казахском',
            'description'       => 'Описание',
            'description_en'    => 'Описание на английском',
            'description_kz'    => 'Описание на казахском',
            'subtitle'          => 'Подзаголовок',
            'subtitle_en'       => 'Подзаголовок на английском',
            'subtitle_kz'       => 'Подзаголовок на казахском',
            'content'           => 'Содержание',
            'content_en'        => 'Содержание на английском',
            'content_kz'        => 'Содержание на казахском',
            'text'              => 'Текст',
            'text_en'           => 'Текст на английском',
            'text_kz'           => 'Текст на казахском',
        ];
    }
}
