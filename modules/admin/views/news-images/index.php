<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изображение новостей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-images-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить изоражение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'news_id',
                'filter' => \app\models\News::getList() ,
                'value' => function ($model) {
                     return
                         Html::a($model->news->name, ['view', 'id' => $model->news->id]);
                },
                'format' => 'raw',
            ],
            'name',
            //'name_en',
            //'name_kz',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100, 'height' => 100]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
