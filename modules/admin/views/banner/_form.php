<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kartik\file\FileInput;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

                <?
                echo $form->field($model, 'text_en')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>


            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'title_kz')->textInput(['maxlength' => true]) ?>

                <?
                echo $form->field($model, 'text_kz')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'menu_id')->dropDownList(\app\models\Menu::getList()) ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


                <?
                echo $form->field($model, 'text')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

                <?= $form->field($model, 'status')->dropDownList([0 => 'Изображение', 1 => 'Видео']) ?>

                <? if($model->id !== null): ?>
                    <div class="row">
                        <div class="col-md-12 col-xs-3" style="text-align: center">
                            <a class="btn btn-default" href="/admin/banner/delete-img?id=<?=$model->id?>" data-method="post" data-confirm="Удалить фотографию?">
                                <span class="glyphicon glyphicon-remove"></span></a>
                            <div style="margin-top:20px;">
                                <img src="<?=$model->getImage()?>" alt="" style="width:200px;">
                            </div>
                        </div>
                    </div>
                <? endif;?>

                <?php
                echo $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <? if($model->id !== null): ?>
                    <div class="row">
                        <div class="col-md-12 col-xs-3" style="text-align: center">
                            <a class="btn btn-default" href="/admin/banner/delete-image-background?id=<?=$model->id?>" data-method="post" data-confirm="Удалить фотографию?">
                                <span class="glyphicon glyphicon-remove"></span></a>
                            <div style="margin-top:20px;">
                                <img src="<?=$model->getImageBackground()?>" alt="" style="width:200px;">
                            </div>
                        </div>
                    </div>
                <? endif;?>

                <?php
                echo $form->field($model, 'image_background')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <?php
                echo $form->field($model, 'video')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'video/*'],
                ]);
                ?>

            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
