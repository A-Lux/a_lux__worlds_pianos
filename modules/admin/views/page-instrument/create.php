<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageInstrument */

$this->title = 'Создать:';
$this->params['breadcrumbs'][] = ['label' => 'Страница инструменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-instrument-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
