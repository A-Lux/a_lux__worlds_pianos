<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sidebar */

$this->title = 'Добавить продукт';
$this->params['breadcrumbs'][] = ['label' => 'Боковое меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sidebar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
