<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use mihaildev\elfinder\ElFinder;
use app\models\Catalog;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryInstruments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="content-tab" data-toggle="tab" href="#content" role="tab" aria-controls="content" aria-selected="false" class="nav-link">Контент</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Мета теги</a>
            </li>
            <li class="nav-item">
                <a id="profile_en-tab" data-toggle="tab" href="#profile_en" role="tab" aria-controls="profile_en" aria-selected="false" class="nav-link">Мета теги на казахском</a>
            </li>
            <li class="nav-item">
                <a id="profile_kz-tab" data-toggle="tab" href="#profile_kz" role="tab" aria-controls="profile_kz" aria-selected="false" class="nav-link">Мета теги на английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile_en" role="tabpanel" aria-labelledby="profile_en-tab" class="tab-pane fade">

                <?= $form->field($model, 'metaName_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_en')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_en')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile_kz" role="tabpanel" aria-labelledby="profile_kz-tab" class="tab-pane fade">
                <?= $form->field($model, 'metaName_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_kz')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_kz')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">
                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>
            </div>
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

                <?
                echo $form->field($model, 'description_en')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]); ?>

                <?if($model->level == 1): ?>
                    <?
                    echo $form->field($model, 'short_content_en')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>

                    <?
                    echo $form->field($model, 'content_en')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>
                <?endif;?>

            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'title_kz')->textInput(['maxlength' => true]) ?>

                <?
                echo $form->field($model, 'description_kz')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]); ?>

                <?if($model->level == 1): ?>
                    <?
                    echo $form->field($model, 'short_content_kz')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>

                    <?
                    echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>
                <?endif;?>

            </div>
            <div id="content" role="tabpanel" aria-labelledby="content-tab" class="tab-pane fade">

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?
                echo $form->field($model, 'description')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]); ?>

                <?if($model->level == 1): ?>
                    <?
                    echo $form->field($model, 'short_content')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>

                    <?
                    echo $form->field($model, 'content')->widget(CKEditor::className(),[
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'full', // basic, standard, full
                            'inline' => false, //по умолчанию false
                        ])
                    ]);
                    ?>
                <?endif;?>


            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="row">
                    <div class="col-md-12 col-xs-3" style="text-align: center">
                        <div style="margin-top:20px;">
                            <img src="<?=$model->getImage()?>" alt="" style="width:200px;">
                        </div>
                    </div>
                </div>
                <?php
                echo $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <?if($model->level == 1): ?>
                    <div class="row">
                        <div class="col-md-12 col-xs-3" style="text-align: center">
                            <div style="margin-top:20px;">
                                <img src="<?=$model->getLogo()?>" alt="" style="width:200px;">
                            </div>
                        </div>
                    </div>
                    <?php
                    echo $form->field($model, 'logo')->widget(FileInput::classname(), [
                        'pluginOptions' => [
                            'showUpload' => false ,
                        ] ,
                        'options' => ['accept' => 'logo/*'],
                    ]);
                    ?>

                    <div class="row">
                        <div class="col-md-12 col-xs-3" style="text-align: center">
                            <div style="margin-top:20px;">
                                <img src="<?=$model->getFonImage()?>" alt="" style="width:200px;">
                            </div>
                        </div>
                    </div>

                    <?php
                    echo $form->field($model, 'fon_image')->widget(FileInput::classname(), [
                        'pluginOptions' => [
                            'showUpload' => false ,
                        ] ,
                        'options' => ['accept' => 'fon_image/*'],
                    ]);
                    ?>
                <? endif;?>

                <? $parent = ArrayHelper::map(\app\models\CategoryInstruments::find()->where('level = 1')->all(), 'id', 'name'); ?>

                <?= $form->field($model, 'parent_id')->dropDownList($parent, ['prompt' => ' ']) ?>

                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно'], ['prompt' => '']) ?>


            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
