<?php

use yii\helpers\Url;
?>
<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $text = "text" . Yii::$app->session["lang"]; ?>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <div class="text">
                    <div class="modal-title">Оставьте заявку на бесплатную консультацию</div>
                    <!-- <div class="modal-subtitle">А так же ответим на все интересующие вопросы</div> -->
                </div>
                <div class="posit-absol">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form action="">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                    <input type="text" placeholder="ВАШЕ ИМЯ" name="Request[name]">
                    <input type="text" placeholder="НОМЕР ТЕЛЕФОНА" name="Request[phone]">
                    <button type="button" class="button request-btn">Узнать цену</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal body -->

</div>

<div class="right-fixed-panel">
    <div class="info-panel">
        <div class="img">
            <img src="/public/images/call-answer.png" alt="">
        </div>
        <div class="info">
            <a href="tel:<?= Yii::$app->view->params['contact']->phone; ?>"><?= Yii::$app->view->params['contact']->phone; ?></a>
        </div>
    </div>
    <div class="info-panel">
        <div class="img">
            <img src="/public/images/002-mail.png" alt="">
        </div>
        <div class="info">
            <a href="mailto:<?= Yii::$app->view->params['contact']->email; ?>"><?= Yii::$app->view->params['contact']->email; ?></a>
        </div>
    </div>
    <div class="info-panel">
        <div class="img">
            <img src="/public/images/003-placeholder.png" alt="">
        </div>
        <div class="info">
            <a href=""><?= Yii::$app->view->params['contact']->address; ?></a>
        </div>
    </div>

    <div class="info-panel hidden-panel">
        <div class="img">
            <img src="/public/images/instagram-white.png" alt="">
        </div>
        <div class="info">
            <a href="<?= Yii::$app->view->params['contact']->instagram; ?>">Наш instagram</a>
        </div>
    </div>

     <div class="info-panel hidden-panel">
        <div class="img">
            <img src="/public/images/facebook-logo.png" alt="">
        </div>
        <div class="info">
            <a href="https://www.facebook.com/worldspianos.kz"> Наш facebook</a>
        </div>
    </div> 
    
</div>
<div class="canvi-navbar">
    <div class="canvi-header-logo">
        <div class="logo">
            <img src="/public/images/WP_black.png" alt="">
        </div>
    </div>
    <div class="canvi-slider">
        <ul class="p-0">
            <? foreach (Yii::$app->view->params['menu'] as $item) : ?>
                <li><a href="<?= $item->url; ?>"><?= $item->$name; ?></a> </li>
            <? endforeach; ?>
        </ul>
        <? foreach (Yii::$app->view->params['sidebar'] as $item) : ?>
            <?php if (!isset($item->products)) continue; ?>
            <div class="item">
                <a href="<?= Url::to(['/catalog/product', 'id' => $item->products->id]) ?>">
                    <div class="img">
                        <img src="<?= $item->products->getImage(); ?>" alt="">
                    </div>
                    <div class="text">
                        <div class="title"><?= $item->products->$name; ?></div>
                        <!-- <div class="subtitle"><?= $item->products->model ?></div> -->
                    </div>
                </a>
            </div>
        <? endforeach; ?>

    </div>
</div>
</div>
<div class="canvi-content">
    <div class="canvi-btn">
        <div class="js-canvi-open-button--left canvi-open-button">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="blur">
        <header>
            <div class="container">
                <div class="row">
                    <ul class="col-lg-10 p-0">
                        <li>
                            <a href="/"><img src="<?= Yii::$app->view->params['logo'][0]->getImage(); ?>" alt=""></a>
                        </li>
                        <? foreach (Yii::$app->view->params['menu'] as $item) : ?>
                            <li><a href="<?= $item->url; ?>"><?= $item->$name; ?></a> </li>
                        <? endforeach; ?>

                    </ul>
                    <div class="soc-net p-0 col-lg-2">
                        <a href="<?= Yii::$app->view->params['contact']->facebook; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" height="20px" viewBox="0 0 96.124 96.123" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z" data-original="#000000" class="active-path" data-old_color="#000000" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <a href="<?= Yii::$app->view->params['contact']->instagram; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" height="20px" viewBox="0 0 97.395 97.395" style="enable-background:new 0 0 97.395 97.395;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M12.501,0h72.393c6.875,0,12.5,5.09,12.5,12.5v72.395c0,7.41-5.625,12.5-12.5,12.5H12.501C5.624,97.395,0,92.305,0,84.895   V12.5C0,5.09,5.624,0,12.501,0L12.501,0z M70.948,10.821c-2.412,0-4.383,1.972-4.383,4.385v10.495c0,2.412,1.971,4.385,4.383,4.385   h11.008c2.412,0,4.385-1.973,4.385-4.385V15.206c0-2.413-1.973-4.385-4.385-4.385H70.948L70.948,10.821z M86.387,41.188h-8.572   c0.811,2.648,1.25,5.453,1.25,8.355c0,16.2-13.556,29.332-30.275,29.332c-16.718,0-30.272-13.132-30.272-29.332   c0-2.904,0.438-5.708,1.25-8.355h-8.945v41.141c0,2.129,1.742,3.872,3.872,3.872h67.822c2.13,0,3.872-1.742,3.872-3.872V41.188   H86.387z M48.789,29.533c-10.802,0-19.56,8.485-19.56,18.953c0,10.468,8.758,18.953,19.56,18.953   c10.803,0,19.562-8.485,19.562-18.953C68.351,38.018,59.593,29.533,48.789,29.533z" data-original="#000000" class="active-path" data-old_color="#000000" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <!-- <a href="<?= Yii::$app->view->params['contact']->twitter; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                             id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                             xml:space="preserve" height="20px">
                <g>
                    <g>
                        <g>
                            <path
                                    d="M512,97.248c-19.04,8.352-39.328,13.888-60.48,16.576c21.76-12.992,38.368-33.408,46.176-58.016    c-20.288,12.096-42.688,20.64-66.56,25.408C411.872,60.704,384.416,48,354.464,48c-58.112,0-104.896,47.168-104.896,104.992    c0,8.32,0.704,16.32,2.432,23.936c-87.264-4.256-164.48-46.08-216.352-109.792c-9.056,15.712-14.368,33.696-14.368,53.056    c0,36.352,18.72,68.576,46.624,87.232c-16.864-0.32-33.408-5.216-47.424-12.928c0,0.32,0,0.736,0,1.152    c0,51.008,36.384,93.376,84.096,103.136c-8.544,2.336-17.856,3.456-27.52,3.456c-6.72,0-13.504-0.384-19.872-1.792    c13.6,41.568,52.192,72.128,98.08,73.12c-35.712,27.936-81.056,44.768-130.144,44.768c-8.608,0-16.864-0.384-25.12-1.44    C46.496,446.88,101.6,464,161.024,464c193.152,0,298.752-160,298.752-298.688c0-4.64-0.16-9.12-0.384-13.568    C480.224,136.96,497.728,118.496,512,97.248z"
                                    data-original="#000000" class="active-path" data-old_color="#000000" />
                        </g>
                    </g>
                </g>
              </svg>
                    </a> -->
                    </div>
                </div>
            </div>
        </header>
    </div>