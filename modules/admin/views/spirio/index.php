<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SpirioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Spirio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spirio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'title',
            //'title_en',
            //'title_kz',
            'subtitle',
            //'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',
            //'subtitle',
            //'subtitle_en',
            //'subtitle_kz',
            //'text:ntext',
            //'text_en:ntext',
            //'text_kz:ntext',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {view}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
