<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */

$this->title = 'Создать изображение новостей';
$this->params['breadcrumbs'][] = ['label' => 'Изображение новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
