<? $name = "name".Yii::$app->session["lang"];?>
<? $title = "title".Yii::$app->session["lang"];?>
<? $content = "content".Yii::$app->session["lang"];?>

<div class="card-main-artist">
    <div class="photo-artist">
        <img src="<?=$artist->getImage();?>" width="100%" alt="">
    </div>


    <div class="instruments-bread">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=Yii::$app->view->params['main'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$artist->$name;?></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="description">
            <?=$artist->$content;?>
        </div>
        <div class="name-artist"><?=$artist->$title;?></div>
    </div>

