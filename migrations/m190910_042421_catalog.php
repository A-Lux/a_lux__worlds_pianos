<?php

use yii\db\Migration;

/**
 * Class m190910_042421_catalog
 */
class m190910_042421_catalog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog}}', [
            'id' => $this->primaryKey(),
            'tree' => $this->integer()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'name' => $this->string(256)->notNull(),
            'name_en' => $this->string(256),
            'name_kz' => $this->string(256),
            'url' => $this->string(256),
            'metaName' => $this->string(256),
            'metaName_en' => $this->string(256),
            'metaName_kz' => $this->string(256),
            'metaDesc' => $this->string(),
            'metaDesc_en' => $this->string(),
            'metaDesc_kz' => $this->string(),
            'metaKey' => $this->string(),
            'metaKey_en' => $this->string(),
            'metaKey_kz' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190910_042421_catalog cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190910_042421_catalog cannot be reverted.\n";

        return false;
    }
    */
}
