<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\CategoryInstruments;
use app\models\search\CategoryInstrumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoryInstrumentsController implements the CRUD actions for CategoryInstruments model.
 */
class CategoryInstrumentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoryInstruments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoryInstrumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoryInstruments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoryInstruments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoryInstruments();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $logo = UploadedFile::getInstance($model, 'logo');
            $fon_image = UploadedFile::getInstance($model, 'fon_image');
            $model->url = $model->generateCyrillicToLatin($model->name, '-');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            if($logo != null) {
                $time = time();
                $logo->saveAs($model->path . $time . '_' . $logo->baseName . '.' . $logo->extension);
                $model->logo = $time . '_' . $logo->baseName . '.' . $logo->extension;
            }

            if($fon_image != null) {
                $time = time();
                $fon_image->saveAs($model->path . $time . '_' . $fon_image->baseName . '.' . $fon_image->extension);
                $model->fon_image = $time . '_' . $fon_image->baseName . '.' . $fon_image->extension;
            }

            if($model->parent_id){
                $model->level = $model->parent->level + 1;
            }else{
                $model->level = 1;
            }

            if($model->save(false)){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CategoryInstruments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;
        $oldLogo = $model->logo;
        $oldFonImage = $model->fon_image;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $logo = UploadedFile::getInstance($model, 'logo');
            $fon_image = UploadedFile::getInstance($model, 'fon_image');
            $model->url = $model->generateCyrillicToLatin($model->name, '-');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    if(file_exists($model->path.$oldImage)) {
                        unlink($model->path . $oldImage);
                    }
                }
            }

            if($logo == null){
                $model->logo = $oldLogo;
            }else{
                $time = time();
                $logo->saveAs($model->path . $time . '_' . $logo->baseName . '.' . $logo->extension);
                $model->logo = $time . '_' . $logo->baseName . '.' . $logo->extension;
                if(!($oldLogo == null)){
                    if(file_exists($model->path.$oldLogo)) {
                        unlink($model->path . $oldLogo);
                    }
                }
            }

            if($fon_image == null){
                $model->fon_image = $oldFonImage;
            }else{
                $time = time();
                $fon_image->saveAs($model->path . $time . '_' . $fon_image->baseName . '.' . $fon_image->extension);
                $model->fon_image = $time . '_' . $fon_image->baseName . '.' . $fon_image->extension;
                if(!($oldFonImage == null)){
                    if(file_exists($model->path.$oldFonImage)) {
                        unlink($model->path . $oldFonImage);
                    }
                }
            }

            if($model->parent_id){
                $model->level = $model->parent->level + 1;
            }else{
                $model->level = 1;
            }

            if($model->save(false)){
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CategoryInstruments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = CategoryInstruments::findOne($id);
        $models = CategoryInstruments::find()->where('sort > '.$model->sort)->all();

        foreach($models as $v){
            $v->sort--;
            $v->save(false);
        }


        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoryInstruments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoryInstruments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoryInstruments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMoveUp($id)
    {
        $model = CategoryInstruments::findOne($id);
        if ($model->sort != 1) {
            $sort = $model->sort - 1;
            $model_down = CategoryInstruments::find()->where("sort = $sort")->one();
            $model_down->sort += 1;
            $model_down->save(false);

            $model->sort -= 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionMoveDown($id)
    {
        $model = CategoryInstruments::findOne($id);
        $model_max_sort = CategoryInstruments::find()->orderBy("sort DESC")->one();

        if ($model->id != $model_max_sort->id) {
            $sort = $model->sort + 1;
            $model_up = CategoryInstruments::find()->where("sort = $sort")->one();
            $model_up->sort -= 1;
            $model_up->save(false);

            $model->sort += 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }
}
