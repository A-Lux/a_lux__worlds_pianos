<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'name_en',
            //'name_kz',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100, 'height' => 100]);
                }
            ],
            //'content:ntext',
            //'content_en:ntext',
            //'content_kz:ntext',
            //'description:ntext',
            //'description_en:ntext',
            //'description_kz:ntext',
            [
                'format' => 'html',
                'attribute' => 'date',
                'value' => function($data){
                    return $data->getDate();
                }
            ],
            //'metaName',
            //'metaName_en',
            //'metaName_kz',
            //'metaDesc:ntext',
            //'metaDesc_en:ntext',
            //'metaDesc_kz:ntext',
            //'metaKey:ntext',
            //'metaKey_en:ntext',
            //'metaKey_kz:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
