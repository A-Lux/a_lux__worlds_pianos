jQuery(document).ready(function ($) {
  $('.slider-first').slick({
    autoplay: false,
    dots: true,
    autoplaySpeed: 2000,
    arrows: false,
  })

  /* Превью картинок 
—---------------------------------------------------— */


  //Видео слайдер
    // $('video').click(function () {
    // console.log('vidik');
  // if ($('.sizing-small').hasClass("state-stopped")) {
    // $('.slick-slide').addClass('.slick-current .slick-active');
  //   console.log('video-stop');
  // };
  // });

  // let videoSlide = $('.sizing-small');

  // if(('video').paused){
  // $('.slick-slide').addClass('.slick-current .slick-active');
  //   console.log('video-stop');
  // }; 

  // if ('video'.paused) {
    // $('.slick-slide').addClass('.slick-current .slick-active');
  //   console.log('video-stop');
  // };
  // $('video').on('pause',function(){
  //   console.log('video-stop')
  // })
  // if($('.sizing-small').hasClass('state-stopped')){
  // $('.slick-slide').addClass('.slick-current .slick-active');
    // console.log('video-stop');
  // }; 

  // Видео слайдер-END


  // Product-text
  $('.img-product img').click(function () {
    console.log($(this).attr('data-name'));
    $('#product-title').html($(this).attr('data-name'));
  });
  $('.piano-product img').click(function () {
    console.log($(this).attr('data-name'));
    $('#piano-title').html($(this).attr('data-name'));
  });


  // Product-text-End







  $('.js-preview').on('click', function () {
    var $this = $(this),
      $img = $this.data('img');

    $('.js-preview').removeClass('active');
    $this.addClass('active');
    $('#js-img-big').attr('src', $img);
    return false;
  });


  function autoplayGallery() {

    var $this = $('.js-preview').filter('.active').next('.js-preview');

    if (!$this.length) {
      $this = $('.js-preview').eq(0);
    }

    var $img = $this.data('img');


    $('.js-preview').removeClass('active');
    $this.addClass('active');
    $('#js-img-big').attr('src', $img);
  }


  // if ($('#js-img-big').length) { 
  // setInterval(autoplayGallery, 5000); 
  // }

  /* Навигация по странице 
  —---------------------------------------------------— */
  var canvi = new Canvi({});

  $('.open-button-mobile-menu').click(function () {
    $('.mobile-menu').addClass('nav-open');
    $('body').addClass('menu-open');
    return false;
  });

  $('.close-mobile-menu').click(function () {
    $('.mobile-menu').removeClass('nav-open');
    $('body').removeClass('menu-open');
    return false;
  });

  $(function () {
    $(document).click(function (event) {
      if ($('.mobile-menu').hasClass('nav-open')) {
        if ($(event.target).closest('.mobile-menu').length) { return; }
        $('.mobile-menu').removeClass('nav-open');
        $('body').removeClass('menu-open');
        event.stopPropagation();
      }
    });
  });
  /* —---------------------------------------------------— */


  $('.product-slider').slick({
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 2000,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 1424,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 1366,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      }, {
        breakpoint: 567,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 300,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  })

  $('.slick-dots').addClass('wrap-slick');

  $(".wrap-slick").wrap("<div class='cont-slick'></div>");

  $(".cont-slick").wrap("<div class='container custom-cont-dots'></div>");


  $('.first-slider').slick({
    dots: true,
    slidesToShow: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    dotsClass: "piano-dots"
  })
});

$('.item').hover(function () {
  console.log('fafkaofkao');
  $('.item').toggleClass('hover-img');
  $(this).removeClass('hover-img');
})

var deadline = new Date($('#time').attr("data-time")).getTime();

var x = setInterval(function () {
  var clock = document.getElementById('countdown');
  var now = new Date().getTime();
  var t = deadline - now;
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((t % (1000 * 60)) / 1000);
  clock.querySelector('.days').innerHTML = days;
  clock.querySelector('.hours').innerHTML = hours;
  clock.querySelector('.minutes').innerHTML = minutes;
  clock.querySelector('.seconds').innerHTML = seconds;
  if (t < 0) {
    clearInterval(x);
    //document.getElementById("demo").innerHTML = "TIME UP";
    clock.querySelector('.days').innerHTML = '0';
    clock.querySelector('.hours').innerHTML = '0';
    clock.querySelector('.minutes').innerHTML = '0';
    clock.querySelector('.seconds').innerHTML = '0';
  }
}, 1000);



