<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_images".
 *
 * @property int $id
 * @property int $news_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 */
class NewsImages extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/news-images/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'name'], 'required'],
            [['news_id'], 'integer'],
            [['name', 'name_en', 'name_kz', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'news_id'   => 'Новость',
            'name'      => 'Подпись',
            'name_en'   => 'Подпись на английском',
            'name_kz'   => 'Подпись на казахском',
            'image'     => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/news-images/' . $this->image : '/no-image.png';
    }

    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    public function getNewsName()
    {
        return (isset($this->news))? $this->news->name:'Не задан';
    }
}
