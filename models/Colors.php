<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "colors".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $image
 */
class Colors extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/colors/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'colors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'type'      => 'Тип',
            'name'      => 'Название',
            'image'     => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/colors/' . $this->image : '/no-image.png';
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getColor()
    {
        return ArrayHelper::map(self::find()->where(['type' => 0])->all(), 'id', 'name');
    }
}
