<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $status
 * @property int $sort
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $url
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'name_en', 'name_kz', 'url', 'metaName', 'metaName_en', 'metaName_kz'], 'string', 'max' => 256],
            [['metaDesc', 'metaDesc_en', 'metaDesc_kz', 'metaKey', 'metaKey_en', 'metaKey_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'name' => 'Название',
            'name_en' => 'Название на английском',
            'name_kz' => 'Название на казахском',
            'url' => 'Url',
            'metaName' => 'Мета Названия',
            'metaName_en' => 'Мета Названия на английском',
            'metaName_kz' => 'Мета Названия на казахском',
            'metaDesc' => 'Мета Описание',
            'metaDesc_en' => 'Мета Описание на английском',
            'metaDesc_kz' => 'Мета Описание на казахском',
            'metaKey' => 'Ключевые слова',
            'metaKey_en' => 'Ключевые слова на английском',
            'metaKey_kz' => 'Ключевые слова на казахском',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = Menu::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
