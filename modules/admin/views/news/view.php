<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту новость?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'name_en',
            'name_kz',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image_banner',
                'value' => function($data){
                    return Html::img($data->getBannerImage(), ['width'=>200]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content_en',
                'value' => function($data){
                    return $data->content_en;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content_kz',
                'value' => function($data){
                    return $data->content_kz;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'description',
                'value' => function($data){
                    return $data->description;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'description_en',
                'value' => function($data){
                    return $data->description_en;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'description_kz',
                'value' => function($data){
                    return $data->description_kz;
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'date',
                'value' => function($data){
                    return $data->getDate();
                }
            ],
            'metaName',
            'metaName_en',
            'metaName_kz',
            'metaDesc:ntext',
            'metaDesc_en:ntext',
            'metaDesc_kz:ntext',
            'metaKey:ntext',
            'metaKey_en:ntext',
            'metaKey_kz:ntext',
        ],
    ]) ?>

</div>
