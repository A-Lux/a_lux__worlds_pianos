<? $name = "name".Yii::$app->session["lang"];?>
<? $title = "title".Yii::$app->session["lang"];?>
<? $content = "content".Yii::$app->session["lang"];?>
<? $description = "description".Yii::$app->session["lang"];?>
<? $text = "text".Yii::$app->session["lang"];?>
<div class="instruments-bread">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><?=Yii::$app->view->params['main'];?></a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$product->$name;?></li>
            </ol>
        </nav>
    </div>
</div>

<div class="product-main">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="photos">
                    <div class="img big-img col-12 col-md-10 p-0">
                        <img src="<?=$product->getImage();?>" alt="" class="img-big" id="js-img-big">
                    </div>

                    <? if(!empty($color)): ?>
                        <div class="description">
                            <div class="material color">Цвет: <span id="product-title">черный полированный</span></div>
                        </div>
                    <? endif;?>
                        <div class="img-thumbs row flex-wrap">
                            <? foreach ($instrumentColor as $item): ?>
                                <? if($item->color->type == 0): ?>
                                    <div class="img img-product">
                                        <img src="<?=$item->color->getImage();?>" alt="" class="active js-preview"
                                             data-img="<?=$item->getImage();?>" data-name="<?=$item->color->name;?>">
                                    </div>
                                <?endif;?>
                            <? endforeach;?>
                        </div>

                    <? if(!empty($material)): ?>
                        <div class="description">
                            <div class="material crown">Материал: <span id="piano-title">КРАСНОЕ ДЕРЕВО</span></div>
                        </div>
                    <? endif;?>
                        <div class="img-thumbs row flex-wrap">
                            <? foreach ($instrumentColor as $item): ?>
                            <? if($item->color->type == 1): ?>
                            <div class="img piano-product">
                                <img src="<?=$item->color->getImage();?>" alt=""
                                     class="active js-preview"
                                     data-img="<?=$item->getImage();?>" data-name="<?=$item->color->name;?>">
                            </div>
                                <?endif;?>
                            <? endforeach;?>
                        </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="info">
                    <div class="title"><?=$product->$name?></div>
                    <div class="subtitle"><?=$product->model;?></div>
                    <div class="product-date">
                        <div class="description-product">Размеры и вес:</div>
                        <ul>
                            <?=$product->height ? '<li>Высота: <span>'.$product->height.' cм</span></li>' : '' ?>
                            <?=$product->width ? '<li>Ширина: <span>'.$product->width.' cм</span></li>' : '' ?>
                            <?=$product->depth ? '<li>Глубина: <span>'.$product->depth.' cм</span></li>' : '' ?>
                            <?=$product->length ? '<li>Длина: <span>'.$product->length.' cм</span></li>' : '' ?>
                            <?=$product->weight ? '<li>Вес: <span>'.$product->weight.' кг</span></li>' : '' ?>
                        </ul>
                    </div>
                    <span class="strip"></span>
                    <div class="text">
                        <?=$product->$description;?>
                    </div>
                    <a href="#" data-toggle="modal" data-target="#myModal" class="button">Узнать цену</a>
                </div>
            </div>
        </div>
        <div class="product-text">
            <span class="strip"></span>
            <? if($product->$content): ?>
                <div class="title">Об инструменте</div>
                <p><?=$product->$content;?></p>
            <?endif;?>
            <span class="strip"></span>
        </div>
    </div>
</div>



<!-- The Modal -->

