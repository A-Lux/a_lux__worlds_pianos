<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sidebar".
 *
 * @property int $id
 * @property int $product_id
 * @property int $sort
 */
class Sidebar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sidebar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'sort'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'product_id'    => 'Продукт',
            'product_image' => 'Изображение продукта',
            'sort'          => 'Sort',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = Sidebar::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getProductsName()
    {
        return (isset($this->products))? $this->products->name:'Не задан';
    }
}
