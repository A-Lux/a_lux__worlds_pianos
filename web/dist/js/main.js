jQuery(document).ready(function ($) {
  $('.slider-first').slick({
    autoplay: true,
    dots: true,
    autoplaySpeed: 2000,
    arrows: false,
  })

  /* Превью картинок 
—---------------------------------------------------— */ 
$('.js-preview').on('click', function(){ 
  var $this = $(this), 
  $img = $this.data('img'); 
  
  $('.js-preview').removeClass('active'); 
  $this.addClass('active'); 
  $('#js-img-big').attr('src', $img); 
  return false; 
  }); 
  
  
  function autoplayGallery() { 
  
  var $this = $('.js-preview').filter('.active').next('.js-preview'); 
  
  if (! $this.length) { 
  $this = $('.js-preview').eq(0); 
  } 
  
  var $img = $this.data('img'); 
  
  
  $('.js-preview').removeClass('active'); 
  $this.addClass('active'); 
  $('#js-img-big').attr('src', $img); 
  } 
  
  
  if ($('#js-img-big').length) { 
  setInterval(autoplayGallery, 5000); 
  }

/* Навигация по странице 
—---------------------------------------------------— */ 
$('.open-button-mobile-menu').click(function () { 
  $('.mobile-menu').addClass('nav-open'); 
  $('body').addClass('menu-open'); 
  return false; 
  }); 
  
  $('.close-mobile-menu').click(function () { 
  $('.mobile-menu').removeClass('nav-open'); 
  $('body').removeClass('menu-open'); 
  return false; 
  }); 
  
  $(function(){ 
  $(document).click(function(event) { 
  if ($('.mobile-menu').hasClass('nav-open')) { 
  if ($(event.target).closest('.mobile-menu').length) { return; } 
  $('.mobile-menu').removeClass('nav-open'); 
  $('body').removeClass('menu-open'); 
  event.stopPropagation(); 
  } 
  }); 
  });
  /* —---------------------------------------------------— */


  $('.product-slider').slick({
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 2000,
    mobileFirst: true,
    responsive: [
              {
                breakpoint: 1424,
                settings: {
                  slidesToShow: 5
                }
              },
              {
                breakpoint: 1366,
                settings: {
                  slidesToShow: 5
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 3
                }
              },{
                breakpoint: 567,
                settings: {
                  slidesToShow: 2
                }
              },{
                breakpoint: 300,
                settings: {
                  slidesToShow: 2
                }
              }
            ]
  })

  $('.slick-dots').addClass('wrap-slick');

  $(".wrap-slick").wrap("<div class='cont-slick'></div>");

  $(".cont-slick").wrap("<div class='container custom-cont-dots'></div>");


  $('.first-slider').slick({
    dots: true,
    slidesToShow: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    dotsClass: "piano-dots"
  })
  
  $('.info-panel').click(function() { 
    $(this).toggleClass('info-panel--open');
  });
});    

  

/*------Timer-------*/
function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}
 
function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');
 
  function updateClock() {
    var t = getTimeRemaining(endtime);
 
    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
 
    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }
 
  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}
 
var deadline = new Date(Date.parse(new Date()) + 1 * 20 * 60 * 60 * 1000); // for endless timer
initializeClock('countdown', deadline);
/*------Timer---------*/


