<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Shourum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shourum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo DatePicker::widget([
        'name'=>'Shourum[date]',
        'language' => 'ru',
        'value' => $model->date,
        'options' => ['placeholder' => 'Выберите дату...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
