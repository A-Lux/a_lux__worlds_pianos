<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $facebook
 * @property string $instagram
 * @property string $whatsap
 * @property string $youtube
 * @property string $twitter
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'email', 'phone', 'facebook', 'instagram', 'whatsap', 'youtube', 'twitter'], 'required'],
            [['address'], 'string'],
            [['phone', 'facebook', 'instagram', 'whatsap', 'youtube', 'twitter'], 'string', 'max' => 255],
            ['email','email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'email' => 'E-mail',
            'phone' => 'Номер телефона',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'whatsap' => 'Whatsapp',
            'youtube' => 'Youtube',
            'twitter' => 'Twitter',
        ];
    }
}
