<? $name = "name" . Yii::$app->session["lang"]; ?>
<? $title = "title" . Yii::$app->session["lang"]; ?>
<? $content = "content" . Yii::$app->session["lang"]; ?>
<? $subtitle = "subtitle" . Yii::$app->session["lang"]; ?>
<? $text = "text" . Yii::$app->session["lang"]; ?>
<?php
    function cutStr($str, $length=30, $postfix='...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }
    ?>
<div class="news-main">
    <div class="photo-artist">
        <img src="<?=$news->getBannerImage();?>" width="100%" alt="">
    </div>


    <div class="instruments-bread">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/"><?= Yii::$app->view->params['main']; ?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?= cutStr($news->$name); ?></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="container page-title">
            <?= $news->$name; ?>
        </div>
    </div>

    <div class="article">
        <div class="container">
            <div class="text width-text">
                <?= $news->$content; ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="news-photo-gallery row">
            <? if(!empty($images)): ?>
                <? foreach($images as $item): ?>
                    <div class="col-md-6 news-photo">
                        <div class="img">
                            <img src="<?=$item->getImage();?>" width="100%" alt="">
                        </div>
                        <div class="description">
                            <?= $item->$name;?>
                        </div>
                    </div>
                <? endforeach;?>
            <? endif;?>
        </div>
    </div>



